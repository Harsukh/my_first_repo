<?php 
include('function/function.php');
date_default_timezone_set('Europe/London');
session_start();
if(isset($_POST['log1']))
{
  if($_FILES['log_file1']['name'] !="")
    {
      if($_POST['log_exp1'] =="")
      {
        $text="Please Select Expiry Date";
        $_SESSION['error']=$text;
        header("Location:../document.php"); /* Redirect browser */
        exit();
      }

      $target_dir = "../../upload/driver_document/vehicle/";
      $filename = explode('.',$_FILES['log_file1']['name']);
      $ext = $filename[1];
      $imgname =date('m-d-y', time()) .'-'. time().'.'.$ext;
      $target_file = $target_dir . $imgname ;

      $uploadOk = 1;
          
      // Check if file already exists
      if (file_exists($target_file)) {
          echo $text="Sorry, file already exists.";
          $uploadOk = 0;
      }
      // Check file size
      if ($_FILES["log_file1"]["size"] > 20000000) {
          echo $text="Sorry, your file is too large.";
          $uploadOk = 0;
      }
      // Allow certain file formats
      if($_FILES['log_file1']['type'] != "image/jpg" && $_FILES['log_file1']['type'] != "image/png" && $_FILES['log_file1']['type'] != "image/jpeg" && $_FILES['log_file1']['type'] != "application/msword" && $_FILES['log_file1']['type'] != "application/pdf") {
          echo $text="Sorry, only JPG, JPEG, PNG , GIF , msword and pdf files are allowed.";
          $uploadOk = 0;
      }
      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk == 0) {
              echo $text="Sorry, your file was not uploaded.";
              $_SESSION['error']=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();
      // if everything is ok, try to upload file
      } else {
          if (move_uploaded_file($_FILES["log_file1"]["tmp_name"], $target_file)) {
              $path=$imgname;
            
            $form_data = array(
             'user_id' => $_POST['did'],
             'name' => 'Log Book Second Page',
             'document' => $path,
             'type' => 'Vehicle',
             'expiry_date' => $_POST['log_exp1'],
            );
              $text="File insert Is complete";
              dbRowInsert('documents', $form_data);
              $_SESSION['Success']=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();


          } else {
              echo $text="Sorry, there was an error uploading your file.";
              $_SESSION['error']=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();
          }
      }
    }else{

          $text="Sorry, Please Select File ....";
          $_SESSION['error']=$text;
          header("Location:../document.php"); /* Redirect browser */
          exit();
      }
      
}
if(isset($_POST['log2']))
{
  if($_FILES['log_file2']['name'] !="")
    {

      if($_POST['log_exp2'] =="")
      {
        $text="Please Select Expiry Date";
        $_SESSION['error']=$text;
        header("Location:../document.php"); /* Redirect browser */
        exit();
      }
      $target_dir = "../../upload/driver_document/vehicle/";
      $filename = explode('.',$_FILES['log_file2']['name']);
      $ext = $filename[1];
      $imgname =date('m-d-y', time()) .'-'. time().'.'.$ext;
      $target_file = $target_dir . $imgname ;

      $uploadOk = 1;
     
      // Check if file already exists
      if (file_exists($target_file)) {
          echo $text="Sorry, file already exists.";
          $uploadOk = 0;
      }
      // Check file size
      if ($_FILES["log_file2"]["size"] > 20000000) {
          echo $text="Sorry, your file is too large.";
          $uploadOk = 0;
      }
      // Allow certain file formats
      if($_FILES['log_file2']['type'] != "image/jpg" && $_FILES['log_file2']['type'] != "image/png" && $_FILES['log_file2']['type'] != "image/jpeg" && $_FILES['log_file2']['type'] != "application/msword" && $_FILES['log_file2']['type'] != "application/pdf") {
          echo $text="Sorry, only JPG, JPEG, PNG , GIF , msword and pdf files are allowed.";
          $uploadOk = 0;
      }
      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk == 0) {
          echo $text="Sorry, your file was not uploaded.";
          $_SESSION['error']=$text;
          header("Location:../document.php"); /* Redirect browser */
          exit();
      // if everything is ok, try to upload file
      } else {
          if (move_uploaded_file($_FILES["log_file2"]["tmp_name"], $target_file)) {
              $path=$imgname;
            
            $form_data = array(
             'user_id' => $_POST['did'],
             'name' => 'Log Book Therd Page',
             'document' => $path,
             'type' => 'Vehicle',
             'expiry_date' => $_POST['log_exp2'],
            );

              $text="File insert Is complete";
              dbRowInsert('documents', $form_data);
              $_SESSION["Success"]=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();


          } else {
              echo $text="Sorry, there was an error uploading your file.";
              $_SESSION['error']=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();
          }
      }
    }else{

          echo $text="Sorry, Please Select File...";
          $_SESSION['error']=$text;
          header("Location:../document.php"); /* Redirect browser */
          exit();
  }
}
if(isset($_POST['insurance']))
{

    if($_POST['insurance_exp'] =="")
      {
        $text="Please Select Expiry Date";
        $_SESSION['error']=$text;
        header("Location:../document.php"); /* Redirect browser */
        exit();
      }
    if($_FILES['insurance_file']['name'] !="")
    {
      $target_dir = "../../upload/driver_document/vehicle/";
      $filename = explode('.',$_FILES['insurance_file']['name']);
      $ext = $filename[1];
      $imgname =date('m-d-y', time()) .'-'. time().'.'.$ext;
      $target_file = $target_dir . $imgname ;

      $uploadOk = 1;
      
      if (file_exists($target_file)) {
          echo $text="Sorry, file already exists.";
          $uploadOk = 0;
      }
      // Check file size
      if ($_FILES["insurance_file"]["size"] > 20000000) {
          echo $text="Sorry, your file is too large.";
          $uploadOk = 0;
      }
      // Allow certain file formats
      if($_FILES['insurance_file']['type'] != "image/jpg" && $_FILES['insurance_file']['type'] != "image/png" && $_FILES['insurance_file']['type'] != "image/jpeg" && $_FILES['insurance_file']['type'] != "application/msword" && $_FILES['insurance_file']['type'] != "application/pdf") {
          echo $text="Sorry, only JPG, JPEG, PNG , GIF , msword and pdf files are allowed.";
          $uploadOk = 0;
      }
      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk == 0) {
          echo $text="Sorry, your file was not uploaded.";
          $_SESSION['error']=$text;
          header("Location:../document.php"); /* Redirect browser */
          exit();
      // if everything is ok, try to upload file
      } else {
          if (move_uploaded_file($_FILES["insurance_file"]["tmp_name"], $target_file)) {
              $path=$imgname;
            
            $form_data = array(
             'user_id' => $_POST['did'],
             'name' => 'Insurance',
             'document' => $path,
             'type' => 'Vehicle',
             'expiry_date' => $_POST['insurance_exp'],
            );

              $text="File insert Is complete";
              dbRowInsert('documents', $form_data);
              $_SESSION["Success"]=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();


          } else {
              echo $text="Sorry, there was an error uploading your file.";
              $_SESSION['error']=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();
          }
      }
     }else{

          echo $text="Sorry, Please Select File...";
          $_SESSION['error']=$text;
          header("Location:../document.php"); /* Redirect browser */
          exit();
    }
}

if(isset($_POST['PCO']))
{
  if($_FILES['pco_file']['name'] !="")
    {

      if($_POST['pco_exp'] =="")
      {
        $text="Please Select Expiry Date";
        $_SESSION['error']=$text;
        header("Location:../document.php"); /* Redirect browser */
        exit();
      }
      $target_dir = "../../upload/driver_document/vehicle/";
      $filename = explode('.',$_FILES['pco_file']['name']);
      $ext = $filename[1];
      $imgname =date('m-d-y', time()) .'-'. time().'.'.$ext;
      $target_file = $target_dir . $imgname ;

      $uploadOk = 1;
      
      // Check if file already exists
      if (file_exists($target_file)) {
          echo $text="Sorry, file already exists.";
          $uploadOk = 0;
      }
      // Check file size
      if ($_FILES["pco_file"]["size"] > 20000000) {
          echo $text="Sorry, your file is too large.";
          $uploadOk = 0;
      }
      // Allow certain file formats
       if($_FILES['pco_file']['type'] != "image/jpg" && $_FILES['pco_file']['type'] != "image/png" && $_FILES['pco_file']['type'] != "image/jpeg" && $_FILES['pco_file']['type'] != "application/msword" && $_FILES['pco_file']['type'] != "application/pdf") {
          echo $text="Sorry, only JPG, JPEG, PNG , GIF , msword and pdf files are allowed.";
          $uploadOk = 0;
      }
      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk == 0) {
          echo $text="Sorry, your file was not uploaded.";
          $_SESSION['error']=$text;
          header("Location:../document.php"); /* Redirect browser */
          exit();
      // if everything is ok, try to upload file
      } else {
          if (move_uploaded_file($_FILES["pco_file"]["tmp_name"], $target_file)) {
              $path=$imgname;
            
            $form_data = array(
             'user_id' => $_POST['did'],
             'name' => 'PCO Certificate',
             'document' => $path,
             'type' => 'Vehicle',
             'expiry_date' => $_POST['pco_exp'],
            );

                $text="File insert Is complete";
                dbRowInsert('documents', $form_data);
                $_SESSION["Success"]=$text;
                header("Location:../document.php"); /* Redirect browser */
                exit();


          } else {
                echo $text="Sorry, there was an error uploading your file.";
                $_SESSION['error']=$text;
                header("Location:../document.php"); /* Redirect browser */
                exit();
          }
      }
     }else{

          echo $text="Sorry, Please Select File...";
          $_SESSION['error']=$text;
          header("Location:../document.php"); /* Redirect browser */
          exit();
    }
}

if(isset($_POST['MOT']))
{
  if($_FILES['mot_file']['name'] !="")
    {

      if($_POST['mot_exp'] =="")
      {
        $text="Please Select Expiry Date";
        $_SESSION['error']=$text;
        header("Location:../document.php"); /* Redirect browser */
        exit();
      }
      $target_dir = "../../upload/driver_document/vehicle/";
      $filename = explode('.',$_FILES['mot_file']['name']);
      $ext = $filename[1];
      $imgname =date('m-d-y', time()) .'-'. time().'.'.$ext;
      $target_file = $target_dir . $imgname ;

      $uploadOk = 1;
      
      // Check if file already exists
      if (file_exists($target_file)) {
          echo $text="Sorry, file already exists.";
          $uploadOk = 0;
      }
      // Check file size
      if ($_FILES["mot_file"]["size"] > 20000000) {
          echo $text="Sorry, your file is too large.";
          $uploadOk = 0;
      }
      // Allow certain file formats
       if($_FILES['mot_file']['type'] != "image/jpg" && $_FILES['mot_file']['type'] != "image/png" && $_FILES['mot_file']['type'] != "image/jpeg" && $_FILES['mot_file']['type'] != "application/msword" && $_FILES['mot_file']['type'] != "application/pdf") {
          echo $text="Sorry, only JPG, JPEG, PNG , GIF , msword and pdf files are allowed.";
          $uploadOk = 0;
      }
      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk == 0) {
          echo $text="Sorry, your file was not uploaded.";
          $_SESSION['error']=$text;
          header("Location:../document.php"); /* Redirect browser */
          exit();
      // if everything is ok, try to upload file
      } else {
          if (move_uploaded_file($_FILES["mot_file"]["tmp_name"], $target_file)) {
              $path=$imgname;
            
            $form_data = array(
             'user_id' => $_POST['did'],
             'name' => 'MOT Certificate',
             'document' => $path,
             'type' => 'Vehicle',
             'expiry_date' => $_POST['mot_exp'],
            );

              $text="File insert Is complete";
              dbRowInsert('documents', $form_data);
              $_SESSION["Success"]=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();


          } else {
              echo $text="Sorry, there was an error uploading your file.";
              $_SESSION['error']=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();
          }
      }
    }else{

          echo $text="Sorry, Please Select File...";
          $_SESSION['error']=$text;
          header("Location:../document.php"); /* Redirect browser */
          exit();
    }
}
?>