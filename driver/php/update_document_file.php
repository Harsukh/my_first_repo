<?php
include('function/function.php');
session_start();
date_default_timezone_set('Europe/London');
if(isset($_POST['DVLA1']))
{
  $dvlaid1=$_POST['dvlaid1'];
  if($_FILES['dvla_file1']['name'] !="")
    {
      if($_POST['dvla_exp1'] =="")
      {
        $text="Please Select Expiry Date";
        $_SESSION['error']=$text;
        header("Location:../document.php"); /* Redirect browser */
        exit();
      }

      $target_dir = "../../upload/driver_document/Personal/";
      $filename = explode('.',$_FILES['dvla_file1']['name']);
      $ext = $filename[1];
      $imgname =date('m-d-y', time()) .'-'. time().'.'.$ext;
      $target_file = $target_dir . $imgname ;

      $uploadOk = 1;
          
      // Check if file already exists
      if (file_exists($target_file)) {
          echo $text="Sorry, file already exists.";
          $uploadOk = 0;
      }
      // Check file size
      if ($_FILES["dvla_file1"]["size"] > 20000000) {
          echo $text="Sorry, your file is too large.";
          $uploadOk = 0;
      }
      // Allow certain file formats
      if($_FILES['dvla_file1']['type'] != "image/jpg" && $_FILES['dvla_file1']['type'] != "image/png" && $_FILES['dvla_file1']['type'] != "image/jpeg" && $_FILES['dvla_file1']['type'] != "application/msword" && $_FILES['dvla_file1']['type'] != "application/pdf") {
          echo $text="Sorry, only JPG, JPEG, PNG , GIF , msword and pdf files are allowed.";
          $uploadOk = 0;
      }
      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk == 0) {
              $_SESSION['error']=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();
      // if everything is ok, try to upload file
      } else {
          if (move_uploaded_file($_FILES["dvla_file1"]["tmp_name"], $target_file)) {
              $path=$imgname;
            
            $form_data = array(
             'document' => $path,
             'type' => 'Personal',
             'status' => '0',
             'expiry_date' => $_POST['dvla_exp1'],
            );

            $q=mysql_query("select * from documents where id='$dvlaid1'");
            $row=mysql_fetch_array($q);
            if($row['document'] !="")
            {
              $dpath="../../upload/driver_document/Personal/".$row['document'];
              unlink($dpath);
            }

              $text="Data Update Is complete";

              dbRowUpdate('documents', $form_data, "WHERE `id` = '$dvlaid1'");
              $_SESSION['Success']=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();


          } else {
              echo $text="Sorry, there was an error uploading your file.";
              $_SESSION['error']=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();
          }
      }
    }else{

          echo $text="Sorry, Please Select File ....";
          $_SESSION['error']=$text;
          header("Location:../document.php"); /* Redirect browser */
          exit();
      }
      
}

if(isset($_POST['DVLA2']))
{
  $dvlaid2=$_POST['dvlaid2'];
  if($_FILES['dvla_file2']['name'] !="")
    {
      if($_POST['dvla_exp2'] =="")
      {
        $text="Please Select Expiry Date";
        $_SESSION['error']=$text;
        header("Location:../document.php"); /* Redirect browser */
        exit();
      }
      $target_dir = "../../upload/driver_document/Personal/";
      $filename = explode('.',$_FILES['dvla_file2']['name']);
      $ext = $filename[1];
      $imgname =date('m-d-y', time()) .'-'. time().'.'.$ext;
      $target_file = $target_dir . $imgname ;

      $uploadOk = 1;
          
      // Check if file already exists
      if (file_exists($target_file)) {
          echo $text="Sorry, file already exists.";
          $uploadOk = 0;

      }
      // Check file size
      if ($_FILES["dvla_file2"]["size"] > 20000000) {
          echo $text="Sorry, your file is too large.";
          $uploadOk = 0;
      }
      // Allow certain file formats
      if($_FILES['dvla_file2']['type'] != "image/jpg" && $_FILES['dvla_file2']['type'] != "image/png" && $_FILES['dvla_file2']['type'] != "image/jpeg" && $_FILES['dvla_file2']['type'] != "application/msword" && $_FILES['dvla_file2']['type'] != "application/pdf") {
          echo $text="Sorry, only JPG, JPEG, PNG , GIF , msword and pdf files are allowed.";
          $uploadOk = 0;
      }
      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk == 0) {
              $_SESSION['error']=$text;
              header("Location:../document.php?dvla1=$text"); /* Redirect browser */
              exit();
      // if everything is ok, try to upload file
      } else {
          if (move_uploaded_file($_FILES["dvla_file2"]["tmp_name"], $target_file)) {
              $path=$imgname;
            
            $form_data = array(
             'document' => $path,
             'type' => 'Personal',
             'status' => '0',
             'expiry_date' => $_POST['dvla_exp2'],
            );

            $q=mysql_query("select * from documents where id='$dvlaid2'");
            $row=mysql_fetch_array($q);
            if($row['document'] !="")
            {
              $dpath="../../upload/driver_document/Personal/".$row['document'];
              unlink($dpath);
            }

              $text="Data Update Is complete";

              dbRowUpdate('documents', $form_data, "WHERE `id` = '$dvlaid2'");
              $_SESSION['Success']=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();


          } else {
              echo $text="Sorry, there was an error uploading your file.";
              $_SESSION['error']=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();
          }
      }
    }else{

          echo $text="Sorry, Please Select File ....";
           $_SESSION['error']=$text;
          header("Location:../document.php"); /* Redirect browser */
          exit();
      }
      
}
if(isset($_POST['PCOB']))
{
  $pcoidb=$_POST['pcoidb'];
  if($_FILES['pco_b_file']['name'] !="")
    {
      if($_POST['pco_b_exp'] =="")
      {
        $text="Please Select Expiry Date";
        $_SESSION['error']=$text;
        header("Location:../document.php"); /* Redirect browser */
        exit();
      }
      $target_dir = "../../upload/driver_document/Personal/";
      $filename = explode('.',$_FILES['pco_b_file']['name']);
      $ext = $filename[1];
      $imgname =date('m-d-y', time()) .'-'. time().'.'.$ext;
      $target_file = $target_dir . $imgname ;

      $uploadOk = 1;
          
      // Check if file already exists
      if (file_exists($target_file)) {
          echo $text="Sorry, file already exists.";
          $uploadOk = 0;
      }
      // Check file size
      if ($_FILES["pco_b_file"]["size"] > 20000000) {
          echo $text="Sorry, your file is too large.";
          $uploadOk = 0;
      }
      // Allow certain file formats
      if($_FILES['pco_b_file']['type'] != "image/jpg" && $_FILES['pco_b_file']['type'] != "image/png" && $_FILES['pco_b_file']['type'] != "image/jpeg" && $_FILES['pco_b_file']['type'] != "application/msword" && $_FILES['pco_b_file']['type'] != "application/pdf") {
          echo $text="Sorry, only JPG, JPEG, PNG , GIF , msword and pdf files are allowed.";
          $uploadOk = 0;
      }
      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk == 0) { 
              $_SESSION['error']=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();
      // if everything is ok, try to upload file
      } else {
          if (move_uploaded_file($_FILES["pco_b_file"]["tmp_name"], $target_file)) {
              $path=$imgname;
            
            $form_data = array(
             'document' => $path,
             'type' => 'Personal',
             'status' => '0',
             'expiry_date' => $_POST['pco_b_exp'],
            );

            $q=mysql_query("select * from documents where id='$pcoidb'");
            $row=mysql_fetch_array($q);
            if($row['document'] !="")
            {
              $dpath="../../upload/driver_document/Personal/".$row['document'];
              unlink($dpath);
            }

              $text="Data Update Is complete";

              dbRowUpdate('documents', $form_data, "WHERE `id` = '$pcoidb'");
              $_SESSION['Success']=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();


          } else {
              echo $text="Sorry, there was an error uploading your file.";
              $_SESSION['error']=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();
          }
      }
    }else{

          echo $text="Sorry, Please Select File ....";
          $_SESSION['error']=$text;
          header("Location:../document.php"); /* Redirect browser */
          exit();
      }
      
}
if(isset($_POST['PCOC']))
{
  $pcoidb=$_POST['pcoidc'];
  if($_FILES['pco_c_file']['name'] !="")
    {
      if($_POST['pco_c_exp'] =="")
      {
        $text="Please Select Expiry Date";
        $_SESSION['error']=$text;
        header("Location:../document.php"); /* Redirect browser */
        exit();
      }
      $target_dir = "../../upload/driver_document/Personal/";
      $filename = explode('.',$_FILES['pco_c_file']['name']);
      $ext = $filename[1];
      $imgname =date('m-d-y', time()) .'-'. time().'.'.$ext;
      $target_file = $target_dir . $imgname ;

      $uploadOk = 1;
          
      // Check if file already exists
      if (file_exists($target_file)) {
          echo $text="Sorry, file already exists.";
          $uploadOk = 0;
      }
      // Check file size
      if ($_FILES["pco_c_file"]["size"] > 20000000) {
          echo $text="Sorry, your file is too large.";
          $uploadOk = 0;
      }
      // Allow certain file formats
      if($_FILES['pco_c_file']['type'] != "image/jpg" && $_FILES['pco_c_file']['type'] != "image/png" && $_FILES['pco_c_file']['type'] != "image/jpeg" && $_FILES['pco_c_file']['type'] != "application/msword" && $_FILES['pco_c_file']['type'] != "application/pdf") {
          echo $text="Sorry, only JPG, JPEG, PNG , GIF , msword and pdf files are allowed.";
          $uploadOk = 0;
      }
      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk == 0) {
              $_SESSION['error']=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();
      // if everything is ok, try to upload file
      } else {
          if (move_uploaded_file($_FILES["pco_c_file"]["tmp_name"], $target_file)) {
              $path=$imgname;
            
            $form_data = array(
             'document' => $path,
             'type' => 'Personal',
             'status' => '0',
             'expiry_date' => $_POST['pco_c_exp'],
            );

            $q=mysql_query("select * from documents where id='$pcoidb'");
            $row=mysql_fetch_array($q);
            if($row['document'] !="")
            {
              $dpath="../../upload/driver_document/Personal/".$row['document'];
              unlink($dpath);
            }

              $text="Data Update Is complete";

              dbRowUpdate('documents', $form_data, "WHERE `id` = '$pcoidb'");
              $_SESSION['Success']=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();


          } else {
              echo $text="Sorry, there was an error uploading your file.";
              $_SESSION['error']=$text;
              header("Location:../document.php"); /* Redirect browser */
              exit();
          }
      }
    }else{

          echo $text="Sorry, Please Select File ....";
          $_SESSION['error']=$text;
          header("Location:../document.php"); /* Redirect browser */
          exit();
      }
      
}
?>