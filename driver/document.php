<?php include('head.php'); ?>
<link href="../assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/pages/css/profile-2.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />

<link href="../assets/global/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    $(document).ready(function(){
     // For A Delete Record Popup
//---------------2--------------
        $('.Resubmit2').click(function() {
            $('body').find("#tab_2_11").attr("class","active");
            $('body').find("#tab_2_33").attr("class","tab-pane");
        });

        $('.11').click(function() {

            $('body').find("#tab_2_11").addClass("class","active");
            $('body').find("#tab_2_33").attr("class","tab-pane");
            $('body').find("#tab_2_22").attr("class","tab-pane");
        });
        $('.22').click(function() {

            $('body').find("#tab_2_22").addClass("class","active");
            $('body').find("#tab_2_33").attr("class","tab-pane");
            $('body').find("#tab_2_11").attr("class","tab-pane");
        });
        $('.23').click(function() {

            $('body').find("#tab_2_33").addClass("class","active");
            $('body').find("#tab_2_22").attr("class","tab-pane");
            $('body').find("#tab_2_11").attr("class","tab-pane");
        });

        $('.vehicle_update').click(function() {
            var id = $(this).attr('data-id');
            var name = $(this).attr('data-name');
            $(".update_vehicle_document").attr("action","php/update_vehicle_document.php");
            $('body').find('.name').append('<input name="##" type="text" value="'+ name +'" class="form-control" id="disabledInput" disabled>');
            $('body').find('.submit_btn').append('<input name="'+ name +'_btn" type="submit" value="Submit" class="btn btn-success">');
            $('body').find('.update_vehicle_document').append('<input name="vdid" type="hidden" value="'+ id +'">');
            $('body').find('.update_vehicle_document').append('<input name="name" type="hidden" value="'+ name +'">');
        });
        $('#close').click(function() {
            $('body').find('.update_vehicle_document').find( "input" ).remove();
        });
         $('.close').click(function() {
            $('body').find('.update_vehicle_document').find( "input" ).remove();
        });
        
    }); 
</script>
<?php include('header.php'); ?>

<?php
    $id=$rowtop['id'];
    $query=mysql_query("SELECT * FROM driver_vehicle_info where user_id=$id");
    $vehicle=mysql_fetch_array($query);
?>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="dashboard.php">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span>Document</span>
                            </li>
                        </ul>
                        
                    </div>
                     <?php if(isset($_SESSION['Success'])!=''){ ?>
                        <div class="alert alert-success fade in" style="margin-top:18px;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <strong>Success!</strong> <?php echo $_SESSION['Success']; ?>
                        </div>
                        <?php 
                        unset($_SESSION["Success"]);
                        ?>
                    <?php } ?>
                    <?php if(isset($_SESSION['error'])!=''){ ?>
                        <div class="alert alert-success fade in" style="margin-top:18px;">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                            <strong>error!</strong> <?php echo $_SESSION['error']; ?>
                        </div>
                        <?php 
                        unset($_SESSION["error"]);
                        ?>
                    <?php } ?>
                    <!-- END PAGE HEADER-->
                    <div class="profile">
                        <div class="tabbable-line tabbable-full-width">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab"> vehicle Detels... </a>
                                </li>
                                <li>
                                    <a href="#tab_1_2" data-toggle="tab"> Driver Detels... </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_1">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-8 profile-info">
                                                    
                                                </div>
                                                <!--end col-md-8-->
                                               
                                                <!--end col-md-4-->
                                            </div>
                                            <!--end row-->
                                            <div class="tabbable-line tabbable-custom-profile">
                                                <ul class="nav nav-tabs">
                                                   
                                                    <li class="active">
                                                        <a href="#tab_1_11" data-toggle="tab"> Add Documents</a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_22" data-toggle="tab"> Approve Documents  </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_33" data-toggle="tab"> Reject Documents </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <!--tab-pane-->
                                                    
                                                    <div class="tab-pane active" id="tab_1_11"> 
                                                        <div class="portlet-body">
                                                        <form action="php/add_vehicle_file.php" method="post" enctype="multipart/form-data">
                                                        <input type="hidden" name="did" value="<?php echo $id; ?>">
                                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Name</th>
                                                                        <th>Document </th>
                                                                        <th>Expiry Date </th>
                                                                        <th>Status </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                    $q=mysql_query("SELECT * FROM documents where user_id='$id' AND type='Vehicle' AND name='Log Book Second Page'");
                                                                    $log=mysql_num_rows($q);
                                                                    $logbook=mysql_fetch_array($q);
                                                                    if($log > 0)
                                                                    { 
                                                                ?>
                                                                     <tr>
                                                                        <td>
                                                                             Log Book Second Page
                                                                        </td>
                                                                        <td> 
                                                                        <?php if($logbook['status'] == 2){ ?>
                                                                            <a href="javascript:;" class="btn blue vehicle_update" data-toggle="modal" data-name="<?php echo $logbook['name']; ?>" data-id="<?php echo $logbook['id']; ?>" data-target="#myModal">
                                                                            <i class="fa fa-file-o"></i> Renew </a>
                                                                        <?php }else{ ?>
                                                                            file is uploded
                                                                        <?php } ?>
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="log_exp1" value="<?php echo $logbook['expiry_date']; ?>" class="form-control" readonly>
                                                                        </td>
                                                                        <td> 
                                                                        <?php if($logbook['status'] == 1){ ?>
                                                                            <i class="fa fa-check btn blue">Approve</i>
                                                                        <?php }elseif($logbook['status'] == 2){ ?>
                                                                            <i class="fa fa-close btn blue">Reject</i>
                                                                        <?php }else{ ?>
                                                                            <button type="button" class="btn btn-warning"><i class="fa fa-hourglass-half"></i> Prosess</button>
                                                                        <?php } ?>
                                                                        </td>
                                                                    </tr>
                                                                <?php }else{ ?>
                                                                     <tr>
                                                                        <td>
                                                                             Log Book Second Page
                                                                        </td>
                                                                        <td> 
                                                                             <span class="btn green fileinput-button">
                                                                                <i class="fa fa-plus"></i>
                                                                                <span> Add files... </span>
                                                                                <input name="log_file1" multiple="" type="file"> 
                                                                            </span>
                                                                        </td>
                                                                        <td> 
                                                                                    <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="log_exp1" value="" />
                                                                        </td>
                                                                        <td> 
                                                                            <button type="submit" class="btn btn-sm grey-salsa btn-outline" name="log1">Submit</button>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                                <?php
                                                                    $q2=mysql_query("SELECT * FROM documents where user_id='$id' AND type='Vehicle' AND name='Log Book Therd Page'");
                                                                    $log2=mysql_num_rows($q2);
                                                                    $logbook2=mysql_fetch_array($q2);
                                                                    if($log2 > 0)
                                                                    { 
                                                                ?>
                                                                     <tr>
                                                                        <td>
                                                                             Log Book Therd Page
                                                                        </td>
                                                                        <td> 
                                                                        <?php if($logbook2['status'] == 2){ ?>
                                                                            <a href="javascript:;" class="btn blue vehicle_update" data-toggle="modal" data-name="<?php echo $logbook2['name']; ?>" data-id="<?php echo $logbook2['id']; ?>" data-target="#myModal">
                                                                            <i class="fa fa-file-o"></i> Renew </a>
                                                                        <?php }else{ ?>
                                                                            file is uploded
                                                                        <?php } ?>
                                                                        </td>
                                                                         <td> 
                                                                            <input type="text" name="log_exp2" value="<?php echo $logbook2['expiry_date']; ?>" class="form-control" readonly>
                                                                        </td>
                                                                        <td> 
                                                                        <?php if($logbook2['status'] == 1){ ?>
                                                                            <i class="fa fa-check btn blue">Approve</i>
                                                                        <?php }elseif($logbook2['status'] == 2){ ?>
                                                                            <i class="fa fa-close btn blue">Reject</i>
                                                                        <?php }else{ ?>
                                                                            <button type="button" class="btn btn-warning"><i class="fa fa-hourglass-half"></i> Prosess</button>
                                                                        <?php } ?>
                                                                        </td>
                                                                    </tr>
                                                                <?php }else{ ?>
                                                                     <tr>
                                                                        <td>
                                                                             Log Book Therd Page
                                                                        </td>
                                                                        <td> 
                                                                             <span class="btn green fileinput-button">
                                                                                <i class="fa fa-plus"></i>
                                                                                <span> Add files... </span>
                                                                                <input name="log_file2" multiple="" type="file"> 
                                                                            </span>
                                                                        </td>
                                                                        <td> 
                                                                            <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="log_exp2" value="" />
                                                                        </td>
                                                                        <td> 
                                                                            <button type="submit" class="btn btn-sm grey-salsa btn-outline" name="log2">Submit</button>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>

                                                                <?php
                                                                    $q3=mysql_query("SELECT * FROM documents where user_id='$id' AND type='Vehicle' AND name='Insurance'");
                                                                    $in=mysql_num_rows($q3);
                                                                    $insurance=mysql_fetch_array($q3);
                                                                    if($in > 0)
                                                                    { 
                                                                ?>
                                                                     <tr>
                                                                        <td>
                                                                             Insurance Certificate
                                                                        </td>
                                                                        <td> 
                                                                        <?php if($insurance['status'] == 2){ ?>
                                                                            <a href="javascript:;" class="btn blue vehicle_update" data-toggle="modal" data-name="<?php echo $insurance['name']; ?>" data-id="<?php echo $insurance['id']; ?>" data-target="#myModal">
                                                                            <i class="fa fa-file-o"></i> Renew </a>
                                                                        <?php }else{ ?>
                                                                            file is uploded
                                                                        <?php } ?>
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="insurance_exp" value="<?php echo $insurance['expiry_date']; ?>" class="form-control" readonly>
                                                                        </td>
                                                                        <td> 
                                                                        <?php if($insurance['status'] == 1){ ?>
                                                                            <i class="fa fa-check btn blue">Approve</i>
                                                                        <?php }elseif($insurance['status'] == 2){ ?>
                                                                            <i class="fa fa-close btn blue">Reject</i>
                                                                        <?php }else{ ?>
                                                                            <button type="button" class="btn btn-warning"><i class="fa fa-hourglass-half"></i> Prosess</button>
                                                                        <?php } ?>
                                                                        </td>
                                                                    </tr>
                                                                <?php }else{ ?>
                                                                     <tr>
                                                                        <td>
                                                                             Insurance
                                                                        </td>
                                                                        <td> 
                                                                            <span class="btn green fileinput-button">
                                                                                <i class="fa fa-plus"></i>
                                                                                <span> Add files... </span>
                                                                                <input name="insurance_file" multiple="" type="file"> 
                                                                            </span>
                                                                        </td>
                                                                        <td> 
                                                                                    <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="insurance_exp" value="" />
                                                                        </td>
                                                                        <td> 
                                                                            <button type="submit" class="btn btn-sm grey-salsa btn-outline" name="insurance">Submit</button>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                                <?php
                                                                    $q4=mysql_query("SELECT * FROM documents where user_id='$id' AND type='Vehicle' AND name='PCO Certificate'");
                                                                    $pcono=mysql_num_rows($q4);
                                                                    $pco=mysql_fetch_array($q4);
                                                                    if($pcono > 0)
                                                                    { 
                                                                ?>
                                                                     <tr>
                                                                        <td>
                                                                             PCO Certificate
                                                                        </td>
                                                                        <td> 
                                                                        <?php if($pco['status'] == 2){ ?>
                                                                            <a href="javascript:;" class="btn blue vehicle_update" data-toggle="modal" data-name="<?php echo $pco['name']; ?>" data-id="<?php echo $pco['id']; ?>" data-target="#myModal">
                                                                            <i class="fa fa-file-o"></i> Renew </a>
                                                                        <?php }else{ ?>
                                                                            file is uploded
                                                                        <?php } ?>
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="pco_exp" value="<?php echo $pco['expiry_date']; ?>" class="form-control" readonly>
                                                                        </td>
                                                                        <td> 
                                                                        <?php if($pco['status'] == 1){ ?>
                                                                            <i class="fa fa-check btn blue">Approve</i>
                                                                        <?php }elseif($pco['status'] == 2){ ?>
                                                                            <i class="fa fa-close btn blue">Reject</i>
                                                                        <?php }else{ ?>
                                                                            <button type="button" class="btn btn-warning"><i class="fa fa-hourglass-half"></i> Prosess</button>
                                                                        <?php } ?>
                                                                        </td>
                                                                    </tr>
                                                                <?php }else{ ?>
                                                                     <tr>
                                                                        <td>
                                                                             PCO Certificate
                                                                        </td>
                                                                        <td> 
                                                                            <span class="btn green fileinput-button">
                                                                                <i class="fa fa-plus"></i>
                                                                                <span> Add files... </span>
                                                                                <input name="pco_file" multiple="" type="file"> 
                                                                            </span>
                                                                        </td>
                                                                        <td> 
                                                                            <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="pco_exp" value="" />
                                                                        </td>
                                                                        <td> 
                                                                            <button type="submit" class="btn btn-sm grey-salsa btn-outline" name="PCO">Submit</button>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                                <?php
                                                                    $q5=mysql_query("SELECT * FROM documents where user_id='$id' AND type='Vehicle' AND name='MOT Certificate'");
                                                                    $motno=mysql_num_rows($q5);
                                                                    $mot=mysql_fetch_array($q5);
                                                                    if($motno > 0)
                                                                    { 
                                                                ?>
                                                                     <tr>
                                                                        <td>
                                                                             MOT Certificate
                                                                        </td>
                                                                        <td> 
                                                                        <?php if($mot['status'] == 2){ ?>
                                                                            <a href="javascript:;" class="btn blue vehicle_update" data-toggle="modal" data-name="<?php echo $mot['name']; ?>" data-id="<?php echo $mot['id']; ?>" data-target="#myModal">
                                                                            <i class="fa fa-file-o"></i> Renew </a>
                                                                        <?php }else{ ?>
                                                                            file is uploded
                                                                        <?php } ?>
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="mot_exp" value="<?php echo $mot['expiry_date']; ?>" class="form-control" readonly>
                                                                        </td>
                                                                        <td> 
                                                                        <?php if($mot['status'] == 1){ ?>
                                                                            <i class="fa fa-check btn blue">Approve</i>
                                                                        <?php }elseif($mot['status'] == 2){ ?>
                                                                            <i class="fa fa-close btn blue">Reject</i>
                                                                        <?php }else{ ?>
                                                                            <button type="button" class="btn btn-warning"><i class="fa fa-hourglass-half"></i> Prosess</button>
                                                                        <?php } ?>
                                                                        </td>
                                                                    </tr>
                                                                <?php }else{ ?>
                                                                     <tr>
                                                                        <td>
                                                                            MOT Certificate
                                                                        </td>
                                                                        <td> 
                                                                            <span class="btn green fileinput-button">
                                                                                <i class="fa fa-plus"></i>
                                                                                <span> Add files... </span>
                                                                                <input name="mot_file" multiple="" type="file"> 
                                                                            </span>
                                                                        </td>
                                                                        <td> 
                                                                            <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="mot_exp" value="" />
                                                                        </td>
                                                                        <td> 
                                                                            <button type="submit" class="btn btn-sm grey-salsa btn-outline" name="MOT">Submit</button>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                                </tbody>
                                                            </table>
                                                            </form>
                                                        </div>
                                                    </div>

                                                    <div class="tab-pane" id="tab_1_22">
                                                        <div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th> Name </th>
                                                                        <th>Type </th>
                                                                        <th>Action </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                    $q11=mysql_query("SELECT * FROM documents where user_id='$id' AND type='Vehicle' AND status='1'");
                                                                    while($documents1=mysql_fetch_array($q11))
                                                                    { 
                                                                ?>
                                                                     <tr>
                                                                        <td>
                                                                             <?php echo $documents1['name']; ?>
                                                                        </td>
                                                                        <td>
                                                                             <?php echo $documents1['type']; ?>
                                                                        </td>
                                                                        <td> 
                                                                            <a class="btn btn-sm grey-salsa btn-outline" open target="_blank" href="../upload/driver_document/vehicle/<?php echo $documents1['document']; ?>"> View </a>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab_1_33">
                                                        <div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Name</th>
                                                                        <th>Type</th>
                                                                        <th style="width: 55%;">Reason</th>
                                                                        <th>Action </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                    $q22=mysql_query("SELECT * FROM documents where user_id='$id' AND type='Vehicle' AND status='2'");
                                                                    while($documents2=mysql_fetch_array($q22))
                                                                    { 
                                                                ?>
                                                                     <tr>
                                                                        <td>
                                                                             <?php echo $documents2['name']; ?>
                                                                        </td>
                                                                        <td> 
                                                                            <?php echo $documents2['type']; ?>
                                                                        </td>
                                                                        <td> 
                                                                            <?php echo $documents2['reason']; ?>
                                                                        </td>
                                                                        <td> 
                                                                            <a class="btn btn-sm grey-salsa btn-outline" open target="_blank" href="../upload/driver_document/vehicle/<?php echo $documents2['document']; ?>"> View </a>
                                                                            <a href="document.php"  class="btn btn-sm grey-salsa btn-outline Resubmit1">Resubmit</button>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!--tab-pane-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab_1_2">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-8 profile-info">
                                                    
                                                </div>
                                                <!--end col-md-8-->
                                               
                                                <!--end col-md-4-->
                                            </div>
                                            <!--end row-->
                                            <div class="tabbable-line tabbable-custom-profile">
                                                <ul class="nav nav-tabs">
                                                   
                                                    <li class="active">
                                                        <a href="#tab_2_11" data-toggle="tab" class="21"> Add Documents</a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_2_22" data-toggle="tab" class="22"> Approve Documents  </a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_2_33" data-toggle="tab" class="23"> Reject Documents </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="tab_2_11">
                                                        <div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                                <form action="php/update_document_file.php" method="post" enctype="multipart/form-data">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Name</th>
                                                                            <th>Document </th>
                                                                            <th>Expiry Date </th>
                                                                            <th>Action </th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <?php
                                                                        //-----------2--1-----------
                                                                        $dq1=mysql_query("SELECT * FROM documents where user_id='$id' AND name='Photocopy of DVLA'");
                                                                        $dvno1=mysql_num_rows($dq1);
                                                                        $dvla1=mysql_fetch_array($dq1);
                                                                        
                                                                        if($dvno1 > 0){ 
                                                                            if($dvla1['status'] == 2){
                                                                    ?>
                                                                    <input type="hidden" name="dvlaid1" value="<?php echo $dvla1['id']; ?>">
                                                                        <tr>
                                                                            <td>
                                                                                 <input type="text" name="dvla1" value="Photocopy of DVLA" class="form-control" readonly>
                                                                            </td>
                                                                            <td> 
                                                                                 <span class="btn green fileinput-button">
                                                                                    <i class="fa fa-plus"></i>
                                                                                    <span> Add files... </span>
                                                                                    <input name="dvla_file1" multiple="" type="file"> 
                                                                                </span>
                                                                            </td>
                                                                            <td> 
                                                                                <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="dvla_exp1" value="" />
                                                                            </td>
                                                                            <td> 
                                                                                <button type="submit" class="btn btn-sm grey-salsa btn-outline" name="DVLA1">Renew</button>
                                                                                <i class="fa fa-close"></i>
                                                                            </td>
                                                                        </tr>

                                                                        <?php }else{ ?>
                                                                            <tr>
                                                                                <td>
                                                                                     <input type="text" name="dvla1" value="Photocopy of DVLA" class="form-control" readonly>
                                                                                </td>
                                                                                <td> 
                                                                                    file is uploded
                                                                                </td>
                                                                                <td> 
                                                                                    <input type="text" name="dvla_exp1" value="<?php echo $dvla1['expiry_date']; ?>" class="form-control" readonly>
                                                                                </td>
                                                                                <td>
                                                                                    <?php if($dvla1['status'] == 1){ ?>
                                                                                        <i class="fa fa-check btn blue">Approve</i>
                                                                                    <?php }else{ ?>
                                                                                        <button type="button" class="btn btn-warning"><i class="fa fa-hourglass-half"></i> Prosess</button>
                                                                                    <?php } ?>
                                                                                </td>
                                                                            </tr>
                                                                    <?php } }else{ ?>
                                                                        <tr>
                                                                            <td>
                                                                                 <input type="text" name="dvla1" value="Photocopy of DVLA" class="form-control" readonly>
                                                                            </td>
                                                                            <td> 
                                                                                 <span class="btn green fileinput-button">
                                                                                    <i class="fa fa-plus"></i>
                                                                                    <span> Add files... </span>
                                                                                    <input name="dvla_file1" multiple="" type="file"> 
                                                                                </span>
                                                                            </td>
                                                                            <td> 
                                                                                <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="dvla_exp1" value="" />
                                                                            </td>
                                                                            <td> 
                                                                                <button type="submit" class="btn btn-sm grey-salsa btn-outline" name="DVLA1">Submit</button>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                    <?php
                                                                        //-----------2-2------------
                                                                        $dq2=mysql_query("SELECT * FROM documents where user_id='$id' AND name='DVLA Counter Part'");
                                                                        $dvno2=mysql_num_rows($dq2);
                                                                        $dvla2=mysql_fetch_array($dq2);
                                                                       if($dvno2 > 0){ 
                                                                            if($dvla2['status'] == 2){
                                                                    ?>
                                                                    <input type="hidden" name="dvlaid2" value="<?php echo $dvla2['id']; ?>">
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" name="dvla2" value="Log Book Therd Page" class="form-control" readonly>
                                                                            </td>
                                                                            <td> 
                                                                                 <span class="btn green fileinput-button">
                                                                                    <i class="fa fa-plus"></i>
                                                                                    <span> Add files... </span>
                                                                                    <input name="dvla_file2" multiple="" type="file"> 
                                                                                </span>
                                                                            </td>
                                                                            <td> 
                                                                                <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="dvla_exp2" value="" />
                                                                            </td>
                                                                            <td> 
                                                                                <button type="submit" class="btn btn-sm grey-salsa btn-outline" name="DVLA2">SRenew</button>
                                                                                <i class="fa fa-close"></i>
                                                                            </td>
                                                                        </tr>
                                                                        <?php }else{ ?>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" name="dvla2" value="Log Book Therd Page" class="form-control" readonly>
                                                                            </td>
                                                                            <td> 
                                                                                file is uploded
                                                                            </td>
                                                                             <td> 
                                                                                <input type="text" name="dvla_exp2" value="<?php echo $dvla2['expiry_date']; ?>" class="form-control" readonly>
                                                                            </td>
                                                                            <td> 
                                                                               <?php if($dvla2['status'] == 1){ ?>
                                                                                    <i class="fa fa-check btn blue">Approve</i>
                                                                                <?php }else{ ?>
                                                                                    <button type="button" class="btn btn-warning"><i class="fa fa-hourglass-half"></i> Prosess</button>
                                                                                <?php } ?>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } }else{ ?>
                                                                         <tr>
                                                                            <td>
                                                                                <input type="text" name="dvla2" value="Log Book Therd Page" class="form-control" readonly>
                                                                            </td>
                                                                            <td> 
                                                                                 <span class="btn green fileinput-button">
                                                                                    <i class="fa fa-plus"></i>
                                                                                    <span> Add files... </span>
                                                                                    <input name="dvla_file2" multiple="" type="file"> 
                                                                                </span>
                                                                            </td>
                                                                            <td> 
                                                                                <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="dvla_exp2" value="" />
                                                                            </td>
                                                                            <td> 
                                                                                <button type="submit" class="btn btn-sm grey-salsa btn-outline" name="DVLA2">Submit</button>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } ?>

                                                                    <?php
                                                                        //-----------2--3-----------
                                                                        $dq3=mysql_query("SELECT * FROM documents where user_id='$id' AND name='PCO Badge'");
                                                                        $pconob=mysql_num_rows($dq3);
                                                                        $pco_Badge=mysql_fetch_array($dq3);
                                                                        if($pconob > 0){ 
                                                                            if($pco_Badge['status'] == 2){
                                                                    ?>
                                                                    <input type="hidden" name="pcoidb" value="<?php echo $pco_Badge['id']; ?>">
                                                                        <tr>
                                                                            <td>
                                                                                 <input type="text" name="pco_b_name" value="Insurance Certificate" class="form-control" readonly>
                                                                            </td>
                                                                            <td> 
                                                                                <span class="btn green fileinput-button">
                                                                                    <i class="fa fa-plus"></i>
                                                                                    <span> Add files... </span>
                                                                                    <input name="pco_b_file" multiple="" type="file"> 
                                                                                </span>
                                                                            </td>
                                                                            <td> 
                                                                                <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="pco_b_exp" value="" />
                                                                            </td>
                                                                            <td> 
                                                                                <button type="submit" class="btn btn-sm grey-salsa btn-outline" name="PCOB">Renew</button>
                                                                                <i class="fa fa-close"></i>
                                                                            </td>
                                                                        </tr>
                                                                        <?php }else{ ?>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" name="pco_b_name" value="Insurance Certificate" class="form-control" readonly>
                                                                            </td>
                                                                            <td> 
                                                                                file is uploded
                                                                            </td>
                                                                            <td> 
                                                                                <input type="text" name="pco_b_exp" value="<?php echo $pco_Badge['expiry_date']; ?>" class="form-control" readonly>
                                                                            </td>
                                                                            <td> 
                                                                               <?php if($pco_Badge['status'] == 1){ ?>
                                                                                    <i class="fa fa-check btn blue">Approve</i>
                                                                                <?php }else{ ?>
                                                                                    <button type="button" class="btn btn-warning"><i class="fa fa-hourglass-half"></i> Prosess</button>
                                                                                <?php } ?>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } }else{ ?>
                                                                         <tr>
                                                                            <td>
                                                                                 <input type="text" name="pco_b_name" value="Insurance Certificate" class="form-control" readonly>
                                                                            </td>
                                                                            <td> 
                                                                                <span class="btn green fileinput-button">
                                                                                    <i class="fa fa-plus"></i>
                                                                                    <span> Add files... </span>
                                                                                    <input name="pco_b_file" multiple="" type="file"> 
                                                                                </span>
                                                                            </td>
                                                                            <td> 
                                                                                        <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="pco_b_exp" value="" />
                                                                            </td>
                                                                            <td> 
                                                                                <button type="submit" class="btn btn-sm grey-salsa btn-outline" name="PCOB">Submit</button>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                    <?php
                                                                        //-----------2--4-----------
                                                                        $dq4=mysql_query("SELECT * FROM documents where user_id='$id' AND name='PCO Counter Part'");
                                                                        $pconoc=mysql_num_rows($dq4);
                                                                        $pco_Counter=mysql_fetch_array($dq4);
                                                                        if($pconoc > 0){ 
                                                                            if($pco_Counter['status'] == 2){
                                                                    ?>
                                                                    <input type="hidden" name="pcoidc" value="<?php echo $pco_Counter['id']; ?>">
                                                                        <tr>
                                                                            <td>
                                                                                 <input type="text" name="pco_c_name" value="Insurance" class="form-control" readonly>
                                                                            </td>
                                                                            <td> 
                                                                                <span class="btn green fileinput-button">
                                                                                    <i class="fa fa-plus"></i>
                                                                                    <span> Add files... </span>
                                                                                    <input name="pco_c_file" multiple="" type="file"> 
                                                                                </span>
                                                                            </td>
                                                                            <td> 
                                                                                <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="pco_c_exp" value="" />
                                                                            </td>
                                                                            <td> 
                                                                                <button type="submit" class="btn btn-sm grey-salsa btn-outline" name="PCOC">SRenew</button>
                                                                                <i class="fa fa-close"></i>
                                                                            </td>
                                                                        </tr>
                                                                        <?php }else{ ?>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="text" name="pco_c_name" value="Insurance" class="form-control" readonly>
                                                                            </td>
                                                                            <td> 
                                                                                file is uploded
                                                                            </td>
                                                                            <td> 
                                                                                <input type="text" name="pco_c_exp" value="<?php echo $pco_Counter['expiry_date']; ?>" class="form-control" readonly>
                                                                            </td>
                                                                            <<td> 
                                                                               <?php if($pco_Counter['status'] == 1){ ?>
                                                                                    <i class="fa fa-check btn blue">Approve</i>
                                                                                <?php }else{ ?>
                                                                                    <button type="button" class="btn btn-warning"><i class="fa fa-hourglass-half"></i> Prosess</button>
                                                                                <?php } ?>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } }else{ ?>
                                                                         <tr>
                                                                            <td>
                                                                                 <input type="text" name="pco_c_name" value="Insurance" class="form-control" readonly>
                                                                            </td>
                                                                            <td> 
                                                                                <span class="btn green fileinput-button">
                                                                                    <i class="fa fa-plus"></i>
                                                                                    <span> Add files... </span>
                                                                                    <input name="pco_c_file" multiple="" type="file"> 
                                                                                </span>
                                                                            </td>
                                                                            <td> 
                                                                                <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="pco_c_exp" value="" />
                                                                            </td>
                                                                            <td> 
                                                                                <button type="submit" class="btn btn-sm grey-salsa btn-outline" name="PCOC">Submit</button>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                    </tbody>
                                                                </form>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <div class="tab-pane" id="tab_2_22">
                                                        <div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th> Name </th>
                                                                        <th>Type </th>
                                                                        <th>Action </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                    $q11=mysql_query("SELECT * FROM documents where user_id=$id AND type='Personal' AND status='1'");
                                                                    while($documents1=mysql_fetch_array($q11))
                                                                    { 
                                                                ?>
                                                                     <tr>
                                                                        <td>
                                                                             <?php echo $documents1['name']; ?>
                                                                        </td>
                                                                        <td>
                                                                             <?php echo $documents1['type']; ?>
                                                                        </td>
                                                                        <td> 
                                                                            <a class="btn btn-sm grey-salsa btn-outline" open target="_blank" href="../upload/driver_document/personal/<?php echo $documents1['document']; ?>"> View </a>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab_2_33">
                                                        <div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Name</th>
                                                                        <th>Type</th>
                                                                        <th style="width: 55%;">Reason</th>
                                                                        <th>Action </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                <?php
                                                                    $q22=mysql_query("SELECT * FROM documents where user_id=$id AND type='Personal' AND status='2'");;
                                                                    while($documents2=mysql_fetch_array($q22))
                                                                    { 
                                                                ?>
                                                                     <tr>
                                                                        <td>
                                                                             <?php echo $documents2['name']; ?>
                                                                        </td>
                                                                        <td> 
                                                                            <?php echo $documents2['type']; ?>
                                                                        </td>
                                                                         <td> 
                                                                            <?php echo $documents2['reason']; ?>
                                                                        </td>
                                                                        <td> 
                                                                            <a class="btn btn-sm grey-salsa btn-outline" open target="_blank" href="../upload/driver_document/personal/<?php echo $documents2['document']; ?>"> View </a>
                                                                            <a   class="btn btn-sm btn-primary Resubmit2">Resubmit</button>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!--tab-pane-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <!-- END QUICK SIDEBAR -->
    <!-- Modal -->
    <form action="" class="update_vehicle_document form-horizontal" method="post" enctype="multipart/form-data">
          <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-md well">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Renew Vehicle Document</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                      <label class="col-sm-3 control-label">Name :</label>
                      <div class="col-sm-9 name">
                        
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="inputPassword" class="col-sm-3 control-label">Document File:</label>
                        <div class="col-sm-9">
                            <span class="btn green fileinput-button">
                                <i class="fa fa-plus"></i>
                                <span> Add files... </span>
                                <input name="files" multiple="" type="file"> 
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-3 control-label">Date :</label>
                        <div class="col-sm-9 date_input">
                            <input class="form-control form-control-inline input-medium date-picker" size="16" type="text" name="exp_date" value="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" class="col-sm-3 control-label"></label>
                        <div class="col-sm-9 submit_btn"> 

                        </div> 
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" id="close" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
    </form>
     <!-- end -->
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
<?php include('footer.php'); ?>
<script src="../assets/pages/scripts/profile.min.js" type="text/javascript"></script>
<script src="../assets/pages/scripts/form-validation.min.js" type="text/javascript"></script>


<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

<script src="../assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>

<script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>



<script src="../assets/global/plugins/jquery-file-upload/js/jquery.fileupload.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery-file-upload/js/jquery.fileupload-process.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery-file-upload/js/jquery.fileupload-validate.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery-file-upload/js/jquery.fileupload-ui.js" type="text/javascript"></script>


