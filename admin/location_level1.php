<?php include('head.php'); ?>
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    $(document).ready(function(){

     // For A Delete Record Popup
        $('.up_l1').click(function() {
            $('body').find('.upload_l1').find( "input" ).remove();
            $('.collapse').addClass('in');
            $('.collapse').attr('aria-expanded','true');
            var id = $(this).attr('data-id');
            var name = $(this).attr('data-name');

            $(".upload_l1").attr("action","php/update_location.php");
            $('body').find('.location_name').append('<input type="text" name="level1" value="'+ name +'" class="form-control" style="width:100%;">');
            $('body').find('.upload_l1').append('<input name="lid" type="hidden" value="'+ id +'">');
        });
        
    }); 
</script>
<?php include('header.php'); ?>
                    
                   
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="dashboard.php">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Pickp Level</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span>1</span>
                            </li>
                        </ul>
                       
                    </div>
                        <?php if(isset($_SESSION['str'])!=''){ ?>
                            <div class="alert alert-success fade in" style="margin-top:18px;">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                <strong>Success!</strong> <?php echo $_SESSION['str']; ?>
                            </div>
                            <?php 
                            unset($_SESSION["str"]);
                            ?>
                        <?php } ?>
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div id="demo" class="collapse">
                                        <form action="php/add_pickup_location.php"  method="post" enctype="multipart/form-data" class="upload_l1">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Name</th>
                                                        <th>Text</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                             Level 1
                                                        </td>
                                                        <td class="location_name">
                                                             <input type="text" name="level1" value="" class="form-control" style="width:100%;">
                                                        </td>
                                                        <td>
                                                             <button type="submit" name="l1" class="btn green">Submit</button>
                                                        </td>
                                                    </tr>        
                                                </tbody>
                                            </table>
                                        </form>
                                    </div>
                                    <div class="caption font-dark">
                                        <a class="btn red btn-outline btn-primary" data-toggle="collapse" href="#demo">Add New Pickup Location Level 1</a>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Text</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $q1=mysql_query("SELECT * FROM pickup_location where level='1'");
                                                $no1=mysql_num_rows($q1);
                                                if(!$no1 > 0){ ?>

                                                    <tr><td colspan="3">No Data Base Selected</td></tr>
                                                <?php }
                                                while($level1=mysql_fetch_array($q1))
                                                {
                                            ?>
                                                <tr>
                                                    <td>
                                                         <?php echo $level1['name']; ?>
                                                    </td>

                                                    <td>
                                                         <?php echo $level1['level']; ?>
                                                    </td>
                                                    <td> 
                                                        <a  class="btn purple up_l1" data-name="<?php echo  $level1['name']; ?>" data-id="<?php echo  $level1['id']; ?>"> Edit <i class="fa fa-edit"></i></a>
                                                        <a href="#" class="btn red"><i class="fa fa-times"></i> Delete</a>
                                                    </td>
                                                </tr>        
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>

                </div>
                <!-- END CONTENT BODY -->
</div>
    <?php include('footer.php'); ?>


<script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>