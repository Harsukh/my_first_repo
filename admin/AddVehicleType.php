<?php include('head.php'); ?>
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="../assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/pages/css/profile-2.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />

<?php include('header.php'); ?>

<?php

if(isset($_POST['tid'])!="" or isset($_GET['tid'])!="" )
{
    if(isset($_POST['tid'])!="")
    {
        $id=$_POST['tid'];
    }
    if(isset($_GET['tid'])!="")
    {
        $id=$_GET['tid'];
    }
    
    $query=mysql_query("SELECT * FROM vehicle_type where id=$id");
    $type=mysql_fetch_array($query);
        $name=$type['name'];
        $passanger_capacity=$type['passanger_capacity'];
        $begs_upto_20kg=$type['begs_upto_20kg'];
        $begs_upto_5kg=$type['begs_upto_5kg'];
        $image=$type['image'];
}
else{
        $name='';
        $passanger_capacity='';
        $begs_upto_20kg='';
        $begs_upto_5kg='';
        $image='';
}
?>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="dashboard.php">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                 <a href="ViewVehicleType.php">Vehicle Type</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span>Add Vehicle Type</span>
                            </li>
                        </ul>
                        
                    </div>
                    <!-- END PAGE HEADER-->
                    <div class="profile">
                        <div class="tabbable-line tabbable-full-width">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab"> vehicle Type Detels... </a>
                                </li>
                            </ul>
                            <?php if(isset($_SESSION['Success'])!=''){ ?>
                                <div class="alert alert-success fade in" style="margin-top:18px;">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                    <strong>Success!</strong> <?php echo $_SESSION['Success']; ?>
                                </div>
                                <?php 
                                unset($_SESSION["Success"]);
                                ?>
                            <?php } ?>
                            <?php if(isset($_SESSION['error'])!=''){ ?>
                                <div class="alert alert-success fade in" style="margin-top:18px;">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                    <strong>error!</strong> <?php echo $_SESSION['error']; ?>
                                </div>
                                <?php 
                                unset($_SESSION["error"]);
                                ?>
                            <?php } ?>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_1">
                                    <form action="php/AddVehicleType.php" method="post" enctype="multipart/form-data" class="getdata">
                                    <input type="hidden" name="vid" value="<?php echo $vehicle['id']; ?>">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group well">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                    <?php if($image!=""){ ?>
                                                        <img src="../upload/vehicle/VehicleTypeimg/<?php echo $image ?>" alt="" /> </div>
                                                    <?php }else{ ?>
                                                         <img src="..\assets\global\img\adduser.png" alt="" /> </div>
                                                    <?php } ?>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"> Select image </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="image"> </span>
                                                        <a href="javascript:;" class="btn default fileinput-exists fileinput-new" data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                                <div class="clearfix margin-top-10">
                                                    <span class="label label-danger">NOTE! </span>
                                                    <span> Maximum Uplode 2MB File</span>
                                                </div>
                                            </div>
                                            <div class="margin-top-10 well">
                                            <?php if(isset($_POST['tid'])!="" or isset($_GET['tid'])!="" ){ ?>
                                                <input type="hidden" name="tid" value="<?php echo $id ?>">
                                                <button class="btn green" style="width:100%;" name="Update" value="Save Changes"> Update </button>
                                            <?php }else{ ?>   
                                                <button class="btn green" style="width:100%;" name="Insert" value="Save Changes"> Insert </button>
                                            <?php } ?>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-8 profile-info">
                                                    
                                                </div>
                                                <!--end col-md-8-->
                                               
                                                <!--end col-md-4-->
                                            </div>
                                            <!--end row-->
                                            <div class="tabbable-line tabbable-custom-profile">
                                                <ul class="nav nav-tabs">
                                                   
                                                    <li class="active">
                                                        <a href="#tab_1_11" data-toggle="tab"> Vehicle Type Information</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <!--tab-pane-->
                                                <div class="tab-pane active" id="tab_1_11">
                                                        <div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th> Filed </th>
                                                                        <th>Descrition </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            Vehicle Type:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="type" value="<?php echo $name ?>" placeholder="Enter Vehicle Registration No..." class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>          
                                                                        <td>
                                                                             passanger capacity:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="passanger_capacity" value="<?php echo $passanger_capacity ?>" placeholder="Enter passanger capacity..." class="form-control passanger">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             capacity of beg (20kg):    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="beg_20kg" value="<?php echo $begs_upto_20kg; ?>" placeholder="Enter capacity of beg (20kg)..." class="form-control 20kg">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             capacity of beg (5kg):    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="beg_5kg" value="<?php echo $begs_upto_5kg; ?>" placeholder="Enter capacity of beg (5kg)..." class="form-control 5kh">
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                    <!--tab-pane-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <!-- END QUICK SIDEBAR -->
        
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
<?php include('footer.php'); ?>
<script src="../assets/pages/scripts/profile.min.js" type="text/javascript"></script>
<script src="../assets/pages/scripts/form-validation.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>