<?php include('head.php'); ?>
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>  
<link href="../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />

<?php include('header.php'); ?>                   
                    <!-- END THEME PANEL -->
                    <!-- BEGIN PAGE BAR -->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <a href="dashboard.php">Home</a>
                                <i class="fa fa-circle"></i>
                            </li>
                            <li>
                                <span>page</span>
                            </li>
                        </ul>
                        <div class="page-toolbar">
                            <div id="dashboard-report-range" class="pull-right tooltips btn btn-sm" data-container="body" data-placement="bottom" data-original-title="Change dashboard date range">
                                <i class="icon-calendar"></i>&nbsp;
                                <span class="thin uppercase hidden-xs"></span>&nbsp;
                                <i class="fa fa-angle-down"></i>
                            </div>
                        </div>
                    </div>
                    <!-- END PAGE BAR -->
                    <!-- BEGIN PAGE TITLE-->
                    <h3 class="page-title"> page
                        <small>Edit</small>
                    </h3>
                     <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXTRAS PORTLET-->
                            <div class="portlet light form-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-green"></i>
                                        <span class="caption-subject font-green bold uppercase">Page Editor</span>
                                    </div>
                                    <div class="actions">
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-cloud-upload"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-wrench"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </div>
                                </div>
<?php
    $id=$_GET['pid'];
    $query=mysql_query("SELECT * FROM page where id='$id'");
    $page=mysql_fetch_array($query);
    
?>
                                <div class="portlet-body form">
                                    <form class="form-horizontal form-bordered" id="mysubmit" method="post" action="php/update_page.php" enctype="multipart/form-data">
                                        <div class="form-body">
                                            <div class="form-group row">
                                                <div class="col-sm-2">
                                                    <h style="font-size: 23px;">page title</h>
                                                </div>
                                                <div class="col-sm-6">
                                                    <input type="text" name="title" value="<?php echo $page['title']; ?>" placeholder="Home" class="form-control" /> 
                                                </div>
                                            </div>
                                            <div class="form-group last">
                                                
                                                    <div name="summernote"  id="summernote_1"><?php echo $page['content']; ?> </div>
                                            
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-2 col-md-10">
                                                <input type="hidden" name="pid" value="<?php echo $page['id']; ?>">
                                                    <button type="submit" class="btn green" id="submit">
                                                        <i class="fa fa-check"></i> Submit</button>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="clearfix"></div>
                    <!-- END DASHBOARD STATS 1-->
                    
                        
                </div>
            </div>
            <!-- END QUICK SIDEBAR -->
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
       <?php include('footer.php'); ?>
        <!-- END THEME LAYOUT SCRIPTS --> 


<script src="../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
<script src="./../assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<script src="../assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.note-codable').attr('name','page_con');
    $('.note-codable').css("height","300px");
    
    $('#submit').on('click',function(){
        $('.note-codable').css("display","block");
        $('.note-editable').css("display","none");
        var text_content = $('.panel-body').html();
        $('.note-codable').html(text_content);

    });
});
</script>