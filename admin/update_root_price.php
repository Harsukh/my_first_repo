<?php include('head.php'); ?>
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function()
{
   
    $(".lev1").click(function()
    {

        var name = $(this).attr('data-name');
        var dataString = 'name='+ name;
        //var=userid=document.getElementsByTagName('user_id').val();
        //var dataString = 'uid='+ userid;
    
        $.ajax
        ({
            type: "POST",
            url: "php/get_level2.php",
            data: dataString,
            cache: false,
            success: function(html)
            {
                $("#lvl2").html(html);
            } 
        });
    });

    $(".lev2").click(function()
    {
        var name = $(this).attr('data-name');
        var dataString = 'name='+ name;
        //var=userid=document.getElementsByTagName('user_id').val();
        //var dataString = 'uid='+ userid;
    
        $.ajax
        ({
            type: "POST",
            url: "php/get_level2.php",
            data: dataString,
            cache: false,
            success: function(html)
            {
                $("#lvl3").html(html);
            } 
        });
    });
    
});
</script>
<style type="text/css">
.slt1{
    color: #fff;
    margin: 5px;
}    
.well{
background: #fff;
padding: 5px;
}
.row{
    background: #fff;
}
</style>
<?php include('header.php'); ?>


                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="dashboard.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <?php if(isset($_GET['arid'])!=""){ ?>
                            <a href="view_active_root.php">View Active Root</a>
                            <?php } if(isset($_GET['drid'])!=""){ ?>
                                <a href="view-deactive-root.php">View Deactive Root</a>
                            <?php } ?>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                        <span>Update Root</span>
                        </li>
                    </ul>
                </div>
                <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN VALIDATION STATES-->
                            <div class="portlet light portlet-fit portlet-form ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-paypal"></i>
                                        <span class="caption-subject font-green bold uppercase">Update Vehicle Root Price...</span>
                                    </div>
                                    
                                </div>
                                <?php if(isset($_SESSION['str'])!=''){ ?>
                                <div class="alert alert-success">
                                    <strong><?php echo $_SESSION['str']; ?></strong>
                                    <?php 
                                    unset($_SESSION["str"]);
                                    ?>
                                </div>
                                <?php } ?>
                                <div class="portlet-body">
                                    <!-- BEGIN FORM-->
                                    <?php
                                        $vikram=0;
                                        if(isset($_GET['arid'])!="")
                                        {
                                            $id=$_GET['arid'];
                                            $vikram=1;
                                        }
                                        if(isset($_GET['drid'])!="")
                                        {
                                             $id=$_GET['drid'];
                                             $vikram=1;
                                        }

                                        if($vikram==1)
                                        {    
                                            $query1=mysql_query("SELECT * FROM price_master where id='$id'");
                                            $up_root=mysql_fetch_array($query1);
                                            $no=mysql_num_rows($query1);
                                            if(!$no > 0)
                                            { 
                                                $_SESSION['str']='Sorry Record Not faund';
                                                ?>
                                                <script type="text/javascript">
                                                // Javascript URL redirection
                                                window.location.replace("view_active_root.php");
                                                </script>
                                        <?php }
                                        } else {  $_SESSION['str']='Sorry Try Again'; ?>
                                            <script type="text/javascript">
                                            // Javascript URL redirection
                                            window.location.replace("view_active_root.php");
                                            </script>
                                    <?php } ?>
                                    <form id="form_sample_2" class="form-horizontal"
                                    method="post" action="php/update_root_price.php">
                                        <div class="form-body row">
                                            <div class="row well">
                                                <label class="control-label col-md-3">Select Pickup Location
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group select2-bootstrap-prepend">
                                                        <div class="input-group-btn">
                                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> Action
                                                                <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                                <?php 
                                                                    $q1=mysql_query("SELECT * FROM pickup_location where level='1'");
                                                                    while($level=mysql_fetch_array($q1))
                                                                    {
                                                                ?>
                                                                <li>
                                                                    <a class="lev1" data-name="<?php echo $level['name']; ?>"><?php echo $level['name']; ?></a>
                                                                </li>
                                                                <?php } ?>
                                                            </ul>
                                                        </div>
                                                        <select name="pickup"  class="form-control select2-multiple" id="lvl2">
                                                            <option value=" " selected="selected">....................Select Pickup Location....................</option>
                                                            <?php 
                                                                $q1=mysql_query("SELECT * FROM pickup_location where level='1'");
                                                                while($level_1=mysql_fetch_array($q1))
                                                                {
                                                            ?>
                                                                <optgroup  label="<?php echo $level_1['name']; ?>">
                                                                    <?php 
                                                                        $name1=$level_1['name'];
                                                                        $q2=mysql_query("SELECT * FROM pickup_location where level='2' AND realetion_ship='$name1'");
                                                                        while($level_2=mysql_fetch_array($q2))
                                                                        {
                                                                    ?>
                                                                        <?php if($name1 == 'All Poscodes') { ?>
                                                                            <option value="HI"  disabled="disabled"><?php echo $level_2['name']; ?></option>
                                                                            <?php 
                                                                                $name2=$level_2['name'];
                                                                                $q3=mysql_query("SELECT * FROM pickup_location where level='3' AND realetion_ship='$name2'");
                                                                                while($level_3=mysql_fetch_array($q3))
                                                                                {
                                                                            ?>
                                                                                <option value="<?php echo $level_3['name']; ?>" <?php if($up_root['pickup_location']==$level_3['name']){echo 'selected'; } ?>><?php echo $level_3['name']; ?></option>
                                                                            <?php } ?>
                                                                                
                                                                        <?php }else{ ?> 
                                                                               <option value="<?php echo $level_2['name']; ?>" <?php if($up_root['pickup_location']==$level_2['name']){echo 'selected'; } ?>><?php echo $level_2['name']; ?></option>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </optgroup>
                                                            <<?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row well">
                                                <label class="control-label col-md-3">Select Pickup Location
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-group select2-bootstrap-prepend">
                                                        <div class="input-group-btn">
                                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"> Action
                                                                <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu">
                                                                <?php 
                                                                    $q11=mysql_query("SELECT * FROM pickup_location where level='1'");
                                                                    while($level1=mysql_fetch_array($q11))
                                                                    {
                                                                ?>
                                                                <li>
                                                                    <a class="lev2" data-name="<?php echo $level1['name']; ?>"><?php echo $level1['name']; ?></a>
                                                                </li>
                                                                <?php } ?>
                                                            </ul>
                                                        </div>
                                                        <select name="dropoff"  class="form-control select2-multiple" id="lvl3">
                                                            <option value=" " selected="selected">....................Select Pickup Location....................</option>
                                                            <?php 
                                                                $q11=mysql_query("SELECT * FROM pickup_location where level='1'");
                                                                while($level_11=mysql_fetch_array($q11))
                                                                {
                                                            ?>
                                                                <optgroup  label="<?php echo $level_11['name']; ?>">
                                                                    <?php 
                                                                        $name1=$level_11['name'];
                                                                        $q22=mysql_query("SELECT * FROM pickup_location where level='2' AND realetion_ship='$name1'");
                                                                        while($level_22=mysql_fetch_array($q22))
                                                                        {
                                                                    ?>
                                                                        <?php if($name1 == 'All Poscodes') { ?>
                                                                            <option value="HI"  disabled="disabled"><?php echo $level_2['name']; ?></option>
                                                                             <?php 
                                                                                $name2=$level_22['name'];
                                                                                $q33=mysql_query("SELECT * FROM pickup_location where level='3' AND realetion_ship='$name2'");
                                                                                while($level_33=mysql_fetch_array($q33))
                                                                                {
                                                                            ?>
                                                                                <option value="<?php echo $level_33['name']; ?>" <?php if($up_root['dropoff_location']==$level_33['name']){echo 'selected'; } ?>><?php echo $level_33['name']; ?></option>
                                                                            <?php } ?>
                                                                                
                                                                        <?php }else{ ?> 
                                                                               <option value="<?php echo $level_22['name']; ?>" <?php if($up_root['dropoff_location']==$level_22['name']){echo 'selected'; } ?>><?php echo $level_22['name']; ?></option>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                </optgroup>
                                                            <<?php } ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                           <div class="form-group well">
                                                <label class="control-label col-md-3">Select State
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <select id="span_small"  class="form-control select2-multiple" id="lvl2"  name="vehicle_type">
                                                        <option value=" ">...Select Vehicle Type...</option>
                                                    <?php 
                                                        $query=mysql_query("SELECT * FROM vehicle_type");
                                                        while($type=mysql_fetch_array($query))
                                                        {
                                                    ?>
                                                        <option value="<?php echo $type['name']; ?>" <?php if($up_root['vehicle_type']==$type['name']){echo 'selected'; } ?>><?php echo $type['name']; ?></option>
                                                    <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group  margin-top-20 well">
                                                <label class="control-label col-md-3">Add Price :
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="text" value="<?php echo $up_root['price']; ?>" class="form-control" name="price" /> </div>
                                                </div>
                                            </div>
                                            <div class="form-group  margin-top-20 well">
                                                <label class="control-label col-md-3">Status :
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-6">
                                                    <div class="input-icon right">
                                                        <i class="fa"></i>
                                                        <input type="checkbox" name="status" value="1" <?php if($up_root['status']==1){echo 'checked'; } ?> /> </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <?php if(isset($_GET['arid'])!=""){ ?>
                                                        <input type="hidden" name="arid" value="<?php echo $up_root['id']; ?>">
                                                    <?php } if(isset($_GET['drid'])!=""){ ?>
                                                         <input type="hidden" name="drid" value="<?php echo $up_root['id']; ?>">
                                                    <?php } ?>
                                                    <button type="submit" class="btn green">Submit</button>
                                                    <a href="view_active_root.php" type="button" class="btn default">View Active Root</a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                            <!-- END VALIDATION STATES-->
                        </div>
                    </div>
<?php include('footer.php'); ?>
<script src="../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>
<script src="../assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>



