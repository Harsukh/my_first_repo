<?php include('head.php'); ?>
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
 <script type="text/javascript">
$(document).ready(function()
{
    $(".getid").click(function()
    {
        var id = $(this).attr('data-id');
        $("#myform").attr("action","AddVehicleType.php");
        $('body').find('#myform').append('<input name="tid" type="hidden" value="'+ id +'">');
        $('#myform').submit();
    });
    
});
</script>
<?php include('header.php'); ?>                    
                   
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="dashboard.php">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span>Vihicle Type</span>
                            </li>
                        </ul>
                       
                    </div>
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <a class="btn red btn-outline btn-primary" href="AddVehicleType.php">Create New Vihicle Type</a>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th> Image </th>
                                                <th> Name </th>
                                                <th> Passanger Capacity </th>
                                                <th> Begs(20kg) </th>
                                                <th> Begs(5kg) </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $query=mysql_query("SELECT * FROM  vehicle_type");
                                                while($vt=mysql_fetch_array($query))
                                                {
                                            ?>
                                            <tr>
                                                <td><img src="../upload/vehicle/VehicleTypeimg/<?php echo $vt['image']; ?>" width="100px"></td>
                                                <td> <?php echo $vt['name']; ?> </td>
                                                <td> <?php echo $vt['passanger_capacity']; ?></td>
                                                <td><?php echo $vt['begs_upto_20kg']; ?></td>
                                                <td><?php echo $vt['begs_upto_5kg']; ?></td>
                                                <td> <a  class="btn purple getid" data-id="<?php echo $vt['id']; ?>"> Edit
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="javascript:;" class="btn red">
                                                        <i class="fa fa-times"></i> Delete
                                                    </a>
                                                </td>
                                            </tr>
        <?php
            }
        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>

                </div>
                <!-- END CONTENT BODY -->
<form action="" method="post" id="myform">
    
</form>
    <?php include('footer.php'); ?>

<script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>