<?php include('head.php'); ?>
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />


<style type="text/css">
.row{
    padding: 10px;
}
</style>

<?php include('header.php'); ?>
                    
                   
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="dashboard.php">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">View Deactive Root</a>
                            </li>
                        </ul>
                       
                    </div>
                    <!-- END PAGE HEADER-->
                    <?php if(isset($_SESSION['str'])!=''){ ?>
                        <div class="alert alert-success">
                            <strong><?php echo $_SESSION['str']; ?></strong>
                            <?php 
                            unset($_SESSION["str"]);
                            ?>
                        </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-title">
                                    <div class="caption font-dark">
                                        <a class="btn red btn-outline btn-primary" href="add_price_root.php">Add New Root Price</a>
                                    </div>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th> Pickup Location</th>
                                                <th> Dropoff Location </th>
                                                <th> Vehicle Type </th>
                                                <th> Price </th>
                                                <th> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                                $query=mysql_query("SELECT * FROM price_master where status='0'");
                                                while($root=mysql_fetch_array($query))
                                                {
                                            ?>
                                            <tr>
                                                <td><?php echo $root['pickup_location']; ?></td>
                                                <td><?php echo $root['dropoff_location']; ?> </td>
                                                <td><?php echo $root['vehicle_type']; ?></td>
                                                <td><?php echo $root['price']; ?></td>
                                                <td> <a href="update_root_price.php?drid=<?php echo $root['id']; ?>" class="btn green"> Edit
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                   <a href="php/delete_root_price.php?drid=<?php echo $root['id']; ?>" class="btn red">
                                                        <i class="fa fa-times"></i> Delete
                                                    </a>
                                                </td>
                                            </tr>
                                            <?php
                                                }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                        </div>
                    </div>

                </div>
                <!-- END CONTENT BODY -->
</div>

 
<?php include('footer.php'); ?>

 <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="../assets/pages/scripts/table-datatables-buttons.min.js" type="text/javascript"></script>