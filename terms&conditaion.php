<?php include("header.php"); ?>
<script src="js/jquery.nouislider.all.min.js"></script>
<script src="js/scripts.js"></script>
<?php include("php/SessionUnset.php"); ?>
<?php include("banner_inner.php"); ?>

<div class="terms container">
<h2>Terms & Conditions</h2>
<h5>General</h5>
<h4>Standard Fare</h4>
<ol>
  <li>We Charge Minimum Fare £20.00 (For Guidelines Only, The Actual Fare May Be Vary**)</li>
  <li>We Charge Booking Fee £1.00 On Top Total Fare On Every Booking Which Made Through Our Site.</li>
  <li>We Charge Waiting Time £20.00 Per Hour.</li>
  <li>We Charge £5.00 Extra On Baby Seat Request Or Booster Seat / Access Luggage.</li>
  <li>Our Additional Charges For Special Vehicles:</li>
  <ul>
    <li>Estate  Cars 		: Fare + £7.00 Extra (may vary or subject to change)</li>
    <li>MPV’s 		: Fare + £15.00 Extra (may vary or subject to change)</li>
    <li>Executives Cars		: Fare + £25.00 Extra (may vary or subject to change)</li>
    <li>Transporters		: Fare*2 - (may vary or subject to change)</li>
  </ul>
  <li>Any Airport Pick, We Charge start from £10.00 Extra Which Is Included 1 Hour Waiting & Parking Charge From 
    The Time Of Flights Landed.</li>
  <li>Courier & Parcel Delivery Fares Will Be - : Fare + £4.00 (Delivery Charge).</li>
  <li>All Payments Can Be Made By Cash Or Cards. Please Note That We Charge 5% Admin Fee Extra On Credit / Debit Card 
    Payments. Customer Is Liable To Pay Additional 5% If They Paying Via PayPal / Any Cards.</li>
  <li>Any Journey That Cancelled By Customer Without Prior Notice, We Charge Cancellation Fee £10.00. If Client Notify Us 
    Minimum 24 Hour Before To Cancel Any Journey, NO CANCELLATION FEE will be charged. </li>
  <li>Corporate Accounts Services</li>
  <ul>
  <li>All Corporate Accounts Subjects to Pay 20% Additional on Top of the Fare on all Invoices. </li>
  <li>Minimum Spending on Account is £200.00 Per Month</li>
  <li>5% Handling / Admin Feel Will Be Applicable.</li>
  <li>Payment Terms:  Within 7 Days of Invoice Date. Fail To Do So, Late Fee May Apply 12.5%</li>
  <li>Invoice Period: 30 days (Every Last Day Of The month)</li>
  </ul>
</ol>
<h5>Insurance</h5>
<p>Our All Cars Are Insured For Passenger Travel (Hire & Reward Insurance). 
  This Insurance Is ForPublic Liability And Does Not Constitute As Travel Insurance.</p>
<h5>Disorderly Behaviours</h5>
<p>We Reserve The Right To Refuse Travel To Anyone Deemed To Be A Nuisance Or Danger To Our Drivers Or Employees. 
  We May Seek The Help Of The Police To Remove Any Offenders From Our Vehicles And 
  Will Not Pay Compensation Or Refund In Such Circumstances.</p>
<h5>Complaints</h5>
<p>If For Some Reason You Are Unhappy With The Service, You Should, In The First Instance, Relay Your Concerns Immediately To The Driver, Who Will Try To Rectify The Issue On The Spot.
  If The Matter Has Not Been Resolved To Your Satisfaction, Please Write To Us At: Airport Transfer Club, 21 Ellington Road, Hounslow East, London – TW3 4HX.</p>
<p>OR</p>
<p>Call Us On 020 7112 8101 / 07515727007.</p>
<p>**Please Note We May Refer Your Complaint To The Public Carriage Office.</p>
<h5>PAYMENT</h5>
<p>Invoices Shall Be Paid In Full Within 7 Days Of Issue Thereof.
  Should Any Invoice Not Be Paid Within 7 Days, Any Outstanding Invoices Shall Immediately Become Due And Payable. 
  It Will Be 12.5% Late Payment Fee Apply.</p>
<p>All Queries Regarding Invoices Should Be Made Within 7 Days Of The Invoice Date.</p>
<p>The Customer Shall Not Be Entitled For Any Reason To Withhold Payment Of Monies Due To The Company And In Particular Shall Not Be
  Entitled To Do So In Circumstances Where The Customer Is In Dispute With The Company And / Or Claims Money Or Compensation 
  From The Company In Respect Of The Services</p>
<h5>CUSTOMER ACCOUNT NUMBER</h5>
<p>The Customer Will Be Issued With An Account Number Which Must Be Quoted On All Bookings.
  Notwithstanding The Aforesaid, The Company Does Not Accept Any Responsibility Whatsoever When Security Account 
  Numbers Are Used By Unauthorised Personnel And / Or For Unauthorised Purposes.</p>
<h5>Cancelations, Claims And Refunds</h5>
<ul>
  <li>Except Cash Bookings, All Credit And Debit Card Booking Will Be Charged at 10% Towards Admin Cost.</li>
  <li>All Late Cancelation Is Subject To Pay Cancelations Fee Which Is £10.00 Minimum.</li>
  <li>Any Bookings / Journey Get Cancelled 24 Hours Prior To Booking Schedule Time Is Consider As Early Cancelation.
    NO CANCELATION FEE WILL BE APPLIED IN SUCH SCENARIO (excl. any fees for credit card payments).</li>
  <li>The Price Provided To The Customer At The Time Of Booking Is Based On The Journey Specified (Original Journey)
    By The Customer At The Time Of Booking. Credit Card Bookings Will Also Include A Booking Fee And VAT.</li>
  <li>All Cash Fare Are VAT Inclusive.</li>
</ul>
<p>All Cash Payments Shall Be Made Directly To The Driver And The Company Acts As Agent For The Driver When Processing The Cash Booking. 
  All Cash Bookings Are Accepted By The Company On Behalf Of The Driver.Please Note All Our Drivers Our Self-Employed, 
  And These Terms Shall Be Considered The Terms Of Trading Between The Driver And The Customer.</p>
<p>The Company Reserves The Right To Charge The Customer For Any Additional Costs Which May Be Incurred By The Company 
  As A Result Of Any Variation From The Original Journey Specified At The Time Of Booking.</p>
<p>Additional Charges May Be Applied At The Sole Discretion Of The Company In The Event Of Any Variation Or Deviations From The Original Journey. 
  The Additional Charges Are Based On The Company's Current Charging Scale Which Can Be Made Available To The Customer On Request.
  Such Variations Or Deviations From The Original Journey In Respect Of Which Additional Charges May Apply Shall Include, Without Limitation,
  Those Set Out Below:</p>
<p>The Customer Will Be Responsible For The Conduct Of All Passenger(S) And Shall Pay For Any Loss And/ Or Damage Caused By Such Passenger(S)
  To The Vehicle Or Any Other Property Of The Company, Including But Not Limited To Cleaning Costs Following Any Spillage Or Soiling
  Of The Vehicle.</p>
<p>If The Booking Is Cancelled Upon Arrival Of The Vehicle To Collect The Customer Of Any Passenger(S) A Cancellation Fee Will Be Payable
  By The Customer To The Company.</p>
<p>The Customer And Any Passenger(S) And Any Luggage Or Personal Items Shall Be Ready For Collection At The Time Stipulated By The Customer 
  When The Booking Is Made. The Company Will Allow 10 Minutes For Waiting Or Loading, When Picking Up The Customer And Passenger(S).
  In The Event That All Customers Have Not Boarded The Vehicle Within 10 Minutes Of The Time Specified At The Time Of Booking 
  The Original Journey Or If No Time Is Specified The Time Of Arrival Of The Vehicle At The Pickup Address The Company 
  Reserves The Right To Charge The Customer For The Total Loading/Waiting Time (For The Avoidance Of Doubt, Including 
  The First 5 Minutes) At Its Current Rate Which Is £20.00 Per Hour.</p>
<p>In Relation To Collections From Airports The Company Will Allow 60 Minutes (Starting From The Last Estimated Arrival Time Known).
  Thereafter The Company Reserves The Right To Charge The Customer For The Waiting/Loading Time After The 60 Minutes.
  Operator Is Solely Responsible To Check Flight Before Allocating To Driver.</p>
<p>In The Event That The Customer Can Request Any Changes During The Course Of The Original Journey, To Make Any Alternative Pick Up(S) 
  Or Collection(S)Of Passenger(S) Or Goods From Other Locations During The Course Of The Original Journey Or Drop Off Any Passenger(S) 
  At Other Locations Other Than Specified In The Original Journey Or To Take Any Variation For The Original Journey Route Specified 
  At The Time Of Booking, Additional Charges May Be Applied By The Company, At Its Then Standard Charge Rate, Which Is Available 
  On Request.</p>
<p>In The Event That The Customer Requires More Than 4 Passengers To Travel In A Vehicle And Has Not Specified This At The Time Of Booking 
  Of The Original Journey Additional Charges May Be Levied By The Company, At Its Then Standard Charge Rates, Which Are Available On 
  Request For The Provision Of A Larger Vehicle Or The Carriage Of Additional Passengers In Excess Of 4.</p>
<p>The Company Shall Provide The Customer With A Quotation For The Carriage Of Passengers And Such Quotation Shall Be Valid For A Period 
  Of 7 Days Or Such Other Period As The Company May Specify. All Bookings Are Subject To Acceptance By The Company And The Company Reserves 
  The Right To Refuse To Accept Any Bookings. All Bookings Are Subject To The Terms And Conditions Of These Conditions, Shall Be Deemed To 
  Be A Separate And Independent Contract And The Company Reserves The Right To Amend Any Booking At Any Time Upon Notice To The Customer.</p>
<p>The Customer Shall At All Times Provide Such Information, Document Or Declaration As May Be Necessary To Enable The Carriage Of 
  The Passenger. The Customer Shall Advise The Company At The Time Of Booking If A Minor Is To Be A Passenger In The Motor Vehicle.</p>
<p>The Company Shall Not Be Liable To The Customer, Whether In Contract, Tort Or By Statute, Or Otherwise In Respect Of Any Loss Of Profits
  And/or For Any Special, Indirect, Incidental Or Consequential Loss Or Damage Suffered By The Customer Howsoever Caused.
  Including Loss Of Business, Profit, Savings, Additional Travel (Including Flights), Use Date Or Information.</p>
<p>All Calls Will Be Monitored For Training And Security Purposes.</p>
<h5>Rating </h5>
<p>Importantly, Every Customer Can Rate The Minicab Driver And Company Once The Trip Has Been Completed. We Monitor This Customer Feedback 
  When Reviewing Its Network Of Minicab Operators. We Follow This Rating To Continue Our Service Towards All Operators. We Reserve 
  All Right to Cancel or Terminate Account / Service of the Operator Based on Customer’s Feedback.</p>
<h5>Termination Of Service </h5>
<p>We Reserve Complete Right And Discretion For Terminating The Service Or Account Which We Offer To Any Operator, If Operator Fail 
  To Comply Company Policy Or Local Authority Laws Or Rules.</p>
<h5>Standard Company Policy To Be Comply</h5>
<ul>
<li>All Operator Must Provide Valid Public Carriage Licence Copy Before Commencing Any Service With Us And Liable To Provide Driver’s 
  Document On Demand.</li>
<li>An Operator Must provide 24/7 Contact Number Along with Email Address for Official Use.</li>
<li>All Operator / Drivers Agreed To Charge Follow The Set Fare By Us Only.  No Overcharge / Any Types Of Unauthorised Charges 
  Will Not Be Accepted In Any Circumstances. Full Refund Will Be Offered To Customer, If Driver Or Operator Overcharged 
  Any Of Our Customers Without Our Consent.</li>
<li>If Operator Has More Than 5 Complains, Our Agreement Of Service Will Be Terminated Immediately.</li>
<li>Please Do Not Sub-Contract Any Journey Without Our Consent As We Must Have A Copy Of The Operator's License Of The Driver 
  Who Is Doing The Journey.</li>
<li>On All Airport Pick Up Always Check Flight Details The Scheduled Landing Time Matches The Flight Number Provided And Track 
  The Flight Before You Commence Your Journey. If You Have Any Doubts Please Call Us Before Going To The Airport As We Can't Defend 
  You If You Go To The Airport Without Checking This Properly. Do Not Presume The Time Entered By The Passenger Is Always Correct.</li>
<li>If Anything Out Of The Ordinary Happens To The Journey Please Inform Us As Soon As Possible So That We Can Add The Notes To 
  The Booking And Act Accordingly.</li>
<li>If Our Client Wants A Return Booking Or Another Journey Organised Please Ask Them To Call Us On +44 7515727007 Or Take The Details
  And Pass Them To Us. We Will Make Contact With The Client And Always Offer You The Booking To Do On Our Behalf. 
  Please Respect Healthy Business Where Do Not Try To Steal The Business Or Client Nor Let Others To Do It. 
  Our Business Relationship Is Based On Trust. Our Client You Are Picking Up On Our Behalf Is Our Customer 
  And All Repeat Business Must Be Directed Back Through To Us</li>
<li>Please Give Our Client Our Business Card If Necessary. Please Email Us If You Need More Of Our Business Cards Sent To You.</li>
<li>When Invoicing Us Or Sending A Sales Receipt Please Email Invoices@Airport-2-London.Com Or Post It To Airport Transfer Club, 
  21 ELLINGTON ROAD, LONDON – TW3 4HX.</li>
<li>Check-List For What Must Be On The Invoice :- Business Name, Business Trading Address, Reference Number Of Each Journey, 
  Amount For Each Journey, Total Amount Payable, Your Bank Account Name If Different To Your Business Name, Your Bank Account Sort 
  Code And Account Number. If You Change Your Bank Details Please Send A Separate Email Stating This As We Will Have To Set 
  This Up With Our Bank And Ensure We Acknowledge That We Have Received This Information From You. Please Invoice Us No More 
  Than Once A Week. To Ensure We Pay You On The Weekly BACS Run Please Make Sure Your Invoice Is Emailed In To Us Before Midnight 
  On Monday For The Journeys You Have Done.</li>
<li>Failure To Inform Us That The Journey Didn't Happen For Whatever Reason Will Result In The Full Fare Being Refunded To 
  The Customer So Please Ensure Your Driver Confirms That The Journey Actually Happened Before You Invoice Us. You Must Provide 
  Car-Park Tickets On Any Airport Pick-Up To Claim ‘NO SHOW BY CUSTOMER’, Fail To Do So, We Will Refund Full Fare To The Customer.</li>
<li>For Non-Urgent Queries Please Email Us As This Keeps The Telephone Lines Clear For Our Clients.</li>
<li>All The Journeys Must Be Completed By A Fully Licensed And Insured Driver And Vehicle. All Operator Is Solely Responsible 
  For Assure And Update Their Documents And Carry Out Regular Checks If Necessary. If Fails To Do So, Our Agreement Will Be 
  Terminated Immediately.</li>
</div>
<?php include("footer.php"); ?>
