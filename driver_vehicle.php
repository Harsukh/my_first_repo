<?php include("header.php"); ?>
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<?php include("php/SessionUnset.php"); ?>
<?php include("banner_inner.php"); ?>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <h2 class="driver">Driver's Information</h2>
      <br />
      <br />
      <h5 class="driver">Vehicle Information</h5>
    </div>
    
    <form id="registerform" class="form-horizontal" role="form" action="#" method="post" enctype="multipart/form-data">
    	<div class="col-md-4">
		</div>
	  <div class="col-md-7 vehicle_main">
	  	<div class="form-group">
          <label>Registration</label>
          <input type="text" name="registration" id="registration" class="form-control" placeholder="Registration" style="width:70%;" required>
        </div>
	    <div class="form-group">
          <label>Make</label>
          <input type="text" class="form-control" name="make" value="" placeholder="Make" style="width:70%;" required>
        </div>
		<div class="form-group">
          <label>Model</label>
          <input type="text" class="form-control" name="model" value="" placeholder="Model" style="width:70%;" required>
        </div>
        
        <div class="form-group">
          <label>Vehicle Type </label>
          <input type="text" class="form-control" name="vehicle_type " value="" placeholder="Vehicle Type" style="width:70%;" required>
        </div>
		<div class="form-group">
          <label>Vehicle Registered Address</label>
          <textarea class="form-control" rows="5" id="vehicle_reg_address" name="vehicle_reg_address" style="width:68%;" placeholder="Vehicle Registered Address" required></textarea>
        </div>
        <div class="form-group">
          <label>Post Code</label>
          <input type="text" class="form-control" name="post_code" value="" placeholder="Post Code" style="width:70%;" required>
        </div>
		  <div class="form-group">
          <label>Insurance Expiry </label>
          <input type="date" name="insurance_expiry" id="insurance_expiry" placeholder="Insurance Expiry" class="form-control input-sm name" style="width:70%;" required>
        </div>
        <div class="form-group">
          <label>MOT Expiry</label>
          <input type="date" name="mot_expiry" id="mot_expiry" placeholder="MOT Expiry" class="form-control input-sm name" style="width:70%;" required>
        </div>
        <h3>Uplaod Document</h3>
        <br />
        <div class="form-group">
          <label>Photocopy of DVLA </label>
          <input name="photo_dvla" type="file" class="file-loading" required>
        </div>
        <div class="form-group">
          <label>DVLA Counter Part </label>
          <input name="photo_dvla_counter" type="file" class="file-loading" required>
        </div>
        <div class="form-group">
          <label>PCO Badge </label>
          <input name="photo_pco" type="file" class="file-loading" required>
        </div>
        <div class="form-group">
          <label>PCO Counter Part </label>
          <input name="photo_pco_counter" type="file" class="file-loading" required>
        </div>
      </div>
	  <div class="col-md-1">
		</div>
      <div class="col-md-12">
        <div class="form-group">
          <center>
            <button type="submit" class="btn btn-default driver_save">Save</button>
          </center>
        </div>
      </div>
	  
    </form>
  </div>
</div>
<?php include("footer.php"); ?>
