                <!-- BEGIN SIDEBAR -->

                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-closed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                        <li class="sidebar-toggler-wrapper hide">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <div class="sidebar-toggler"> </div>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                        </li>
                        <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
                        
                        <li class="nav-item start active open">
                            <a href="dashboard.php" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Dashboard</span>
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                            </a>
                           
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-settings"></i>
                                <span class="title">Seting</span>
                                <span class="arrow"></span>
                            </a>
                             <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="logo.php" class="nav-link">Logo</a>
                                </li>
                                <li class="nav-item">
                                    <a href="copyright.php" class="nav-link">copyright</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title">User</span>
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="view_user.php" class="nav-link">View User</a>
                                </li>
                                <li class="nav-item">
                                    <a href="add_user.php" class="nav-link">Add New User</a>
                                </li>
                            </ul>
                            
                        </li>
                        <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-support"></i>
                                <span class="title">Driver</span>
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="view_driver.php" class="nav-link">View Driver</a>
                                </li>
                            </ul>
                            
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-taxi"></i>
                                <span class="title">Vehicle</span>
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="view_vehicle.php" class="nav-link">View Driver Vehicle</a>
                                </li>
                                <li class="nav-item">
                                    <a href="ViewVehicleType.php" class="nav-link">View Vehicle Type</a>
                                </li>
                                <li class="nav-item">
                                    <a href="AddVehicleType.php" class="nav-link">Add Vehicle Type</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-map-marker"></i>
                                <span class="title">Pickup Location</span>
                                <span class="selected"></span>
                                <span class="arrow open"></span>  
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="location_level1.php" class="nav-link">Pickup Location Level 1</a>
                                </li>
                                <li class="nav-item">
                                    <a href="location_level2.php" class="nav-link">Pickup Location Level 2</a>
                                </li>
                                <li class="nav-item">
                                    <a href="location_level3.php" class="nav-link">Pickup Location Level 3</a>
                                </li>
                            </ul>
                        </li>
                        
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-paypal"></i>
                                <span class="title">Price Master</span>
                                <span class="selected"></span>
                                <span class="arrow open"></span>  
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="add_price_root.php" class="nav-link">Add Root Price</a>
                                </li>
                                <li class="nav-item">
                                    <a href="view_active_root.php" class="nav-link">View Active Root</a>
                                </li>
                                <li class="nav-item">
                                    <a href="view-deactive-root.php" class="nav-link">View Deactive Root</a>
                                </li>
                                <li class="nav-item">
                                    <a href="SpecialDatePrice.php" class="nav-link">Special Date Price</a>
                                </li>
                            </ul>
                        </li>
                         <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-link"></i>
                                <span class="title">Share Link Management</span>
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                                
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="ViewShareLink.php" class="nav-link">View Share Link</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-file-code-o"></i>
                                <span class="title">Page</span>
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                                
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="view_page.php" class="nav-link">All Page</a>
                                </li>
                                <li class="nav-item">
                                    <a href="add_page.php" class="nav-link">Create New Page</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-settings"></i>
                                <span class="title">System</span>
                                <span class="arrow"></span>
                            </a>
                            
                        </li>
                        <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-folder"></i>
                                <span class="title">Multi Level Menu</span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <i class="icon-settings"></i> Item 1
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item">
                                            <a href="javascript:;" target="_blank" class="nav-link">
                                                <i class="icon-user"></i> Arrow Toggle
                                                <span class="arrow nav-toggle"></span>
                                            </a>
                                            <ul class="sub-menu">
                                                <li class="nav-item">
                                                    <a href="#" class="nav-link">
                                                        <i class="icon-power"></i> Sample Link 1</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="#" class="nav-link">
                                                        <i class="icon-paper-plane"></i> Sample Link 1</a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="#" class="nav-link">
                                                        <i class="icon-star"></i> Sample Link 1</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                <i class="icon-camera"></i> Sample Link 1</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                <i class="icon-link"></i> Sample Link 2</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                <i class="icon-pointer"></i> Sample Link 3</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="javascript:;" target="_blank" class="nav-link">
                                        <i class="icon-globe"></i> Arrow Toggle
                                        <span class="arrow nav-toggle"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                <i class="icon-tag"></i> Sample Link 1</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                <i class="icon-pencil"></i> Sample Link 1</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="#" class="nav-link">
                                                <i class="icon-graph"></i> Sample Link 1</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        <i class="icon-bar-chart"></i> Item 3 </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- END SIDEBAR MENU -->
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
    