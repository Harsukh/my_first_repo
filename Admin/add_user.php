<?php include('head.php'); ?>
<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<link href="../assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
 <link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<?php include('header.php'); ?>
                <div class="page-bar">
                    <ul class="page-breadcrumb">
                        <li>
                            <a href="dashboard.php">Home</a>
                            <i class="fa fa-circle"></i>
                        </li>
                        <li>
                            <span>Add User</span>
                        </li>
                    </ul>
                </div>
                <?php if(isset($_SESSION['Success'])!=''){ ?>
                    <div class="alert alert-success fade in" style="margin-top:18px;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                        <strong>Success!</strong> <?php echo $_SESSION['Success']; ?>
                    </div>
                    <?php 
                    unset($_SESSION["Success"]);
                    ?>
                <?php } ?>
                <?php if(isset($_SESSION['error'])!=''){ ?>
                    <div class="alert alert-success fade in" style="margin-top:18px;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                        <strong>error!</strong> <?php echo $_SESSION['error']; ?>
                    </div>
                    <?php 
                    unset($_SESSION["error"]);
                    ?>
                <?php } ?>
                <div class="row">
                        <div class="col-md-12">
                            <!-- BEGIN VALIDATION STATES-->
                            <div class="portlet light portlet-fit portlet-form ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="icon-user"></i>
                                        <span class="caption-subject font-green bold uppercase">Add New User...</span>
                                    </div>
                                    
                                </div>
                                <div class="portlet-body">
                                    <!-- BEGIN FORM-->
                                    <form action="php/add_user_data.php" id="form_sample_2" class="form-horizontal" method="post" >
                                        <div class="form-body">
                                            <div class="alert alert-danger display-hide">
                                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                                            <div class="alert alert-success display-hide">
                                                <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">First Name</label>
                                                <div class="input-group col-md-4 form-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-user"></i>
                                                    </span>
                                                    <input type="text" class="form-control" name="firstname" />
                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="control-label col-md-3">Last Name</label>
                                                <div class="input-group col-md-4 form-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-user"></i>
                                                    </span>
                                                    <input type="text" class="form-control" name="lastname" />
                                                </div>
                                            </div> 
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Email Address
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="input-group col-md-4 form-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-envelope"></i>
                                                    </span>
                                                    <input  type="text" class="form-control" name="email" /> 
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Password
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="input-group col-md-4 form-group">
                                                    <input type="password" class="form-control" name="password" id="exampleInputPassword1" placeholder="Password">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-user font-red"></i>
                                                    </span>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Mobile
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="input-group col-md-4 form-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-mobile"></i>
                                                    </span>
                                                        <input type="text" class="form-control" name="mobile" /> </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Gender :
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <div class="radio-list" data-error-container="#form_2_membership_error">
                                                        <label>
                                                            <input type="radio" name="gender" value="male" /> Male </label>
                                                        <label>
                                                            <input type="radio" name="gender" value="female" /> Female </label>
                                                    </div>
                                                    <div id="form_2_membership_error"> </div>
                                                </div>
                                            </div>
                                            <div class="form-group  margin-top-20">
                                                <label class="control-label col-md-3">1st line address
                                                </label>
                                                <div class="input-group col-md-4 form-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-crosshairs"></i>
                                                    </span>
                                                        <input type="text" class="form-control" name="address1" />
                                                </div>
                                            </div>
                                            <div class="form-group  margin-top-20">
                                                <label class="control-label col-md-3">2st line address
                                                </label>
                                                <div class="input-group col-md-4 form-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-crosshairs"></i>
                                                    </span>
                                                        <input type="text" class="form-control" name="address2" />
                                                </div>
                                            </div>
                                            <div class="form-group  margin-top-20">
                                                <label class="control-label col-md-3">County :
                                                </label>
                                                <div class="input-group col-md-4 form-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-crosshairs"></i>
                                                    </span>
                                                        <input type="text" class="form-control" name="county" />
                                                </div>
                                            </div>
                                            <div class="form-group  margin-top-20">
                                                <label class="control-label col-md-3">Town :
                                                </label>
                                                <div class="input-group col-md-4 form-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-crosshairs"></i>
                                                    </span>
                                                        <input type="text" class="form-control" name="town" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Postcode
                                                    <span class="required"> * </span>
                                                </label>
                                               <div class="input-group col-md-4 form-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-map-signs"></i>
                                                    </span>
                                                        <input type="text" class="form-control" name="pin" />
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">User Rool
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="input-group">
                                                    <span class="input-group-addon">
                                                        <i class="fa fa-user"></i>
                                                    </span>
                                                    <select class="form-control select2me" name="usertype" name="city">
                                                        <option value=" "> ..........select User Rool..........</option>
                                                        <option value="admin">Admin</option>
                                                        <option value="individualuser">Individual user</option>
                                                        <option value="corporateuser">Corporate user</option>
                                                        <option value="driver">Driver</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">Admin Agree..
                                                    <span class="required"> * </span>
                                                </label>
                                                <div class="col-md-4">
                                                    <div class="checkbox-list" data-error-container="#form_2_services_error">
                                                        <label>
                                                            <input type="checkbox" value="1" name="agree" /></label>
                                                    </div>
                                                    <div id="form_2_services_error"> </div>
                                                </div>
                                            </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                   
                                                    <button type="submit" class="btn green">Submit</button>
                                                    <button type="button" class="btn default">Cancel</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                            <!-- END VALIDATION STATES-->
                        </div>
                    </div>
<?php include('footer.php'); ?>

<script src="../assets/pages/scripts/profile.min.js" type="text/javascript"></script>
<script src="../assets/pages/scripts/form-validation.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/select2/js/select2.full.min.js"></script>
<script src="../assets/pages/scripts/components-select2.min.js"></script>
<script src="../assets/pages/scripts/components-date-time-pickers.min.js"></script>
<script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="../assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
<script src="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>