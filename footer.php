<section id="bottom">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h3 class="header header-bottom">YOLO!</h3>
        <p> An Emerging Airport Transfer Club Which Is Available 24/7 At Your Service To Fulfill Your Frequent Journey Requirement At Very Competitive Rates nbsp;&nbsp;&nbsp;&nbsp;<a href="about.php" class="about_readmore">Read More.....</a> </p>
      </div>
      <div class="col-md-3">
        <h3 class="header header-bottom">DOWNLOADS</h3>
        <p> Now you can download our app on every mobile device: </p>
        <p> <a href="#" class="btn btn-primary"><i class="uk-icon-apple"></i></a> <a href="#" class="btn btn-primary"><i class="uk-icon-android"></i></a> <a href="#" class="btn btn-primary"><i class="uk-icon-windows"></i></a> </p>
      </div>
      <div class="col-md-3">
        <h3 class="header header-bottom">MORE STUFF</h3>
        <p> <i class="uk-icon-check "></i>&nbsp; <a href="#">How it Works</a><br>
          <i class="uk-icon-check "></i>&nbsp; <a href="#">Site Maps </a><br>
          <i class="uk-icon-check "></i>&nbsp; <a href="terms&conditaion.php">Terms & Condition </a><br>
          <i class="uk-icon-check "></i>&nbsp; <a href="privacy-policy.php">Privacy Policy</a> <br />
		 <i class="uk-icon-check "></i>&nbsp; <a href="Coverage-area.php">Coverage - Area</a></p>
      </div>
      <div class="col-md-3">
        <h3 class="header header-bottom">CONTACT US</h3>
        <p> <i class="uk-icon-envelope "></i>&nbsp; taxi (@) yoursite.com<br>
          <i class="uk-icon-phone "></i>&nbsp; +123.456.789<br>
          <i class="uk-icon-print "></i>&nbsp; +123.456.789<br>
          <i class="uk-icon-building "></i>&nbsp; address </p>
      </div>
    </div>
  </div>
</section>
<section id="bottom">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <h3 class="header header-bottom">SERVICES</h3>
        <p>
          <i class="uk-icon-check "></i>&nbsp; <a href="#">Transfers</a><br>
          <i class="uk-icon-check "></i>&nbsp; <a href="#">Directory</a><br>
          <i class="uk-icon-check "></i>&nbsp; <a href="FlightLiveArrivals.php">Flight live arrivals</a> 
        </p>
      </div>
      <div class="col-md-3">
        <h3 class="header header-bottom">OUR VEHICLES</h3>
        <p> <i class="uk-icon-check "></i>&nbsp; <a href="#">Private Hire Taxis</a><br>
          <i class="uk-icon-check "></i>&nbsp; <a href="#">Business vehicles</a><br>
          <i class="uk-icon-check "></i>&nbsp; <a href="#">Minibuses & Coaches</a></p>
      </div>
      <div class="col-md-3">
        <h3 class="header header-bottom">OUR TRANSFERS</h3>
        <p> <i class="uk-icon-check "></i>&nbsp; <a href="#">Airport transfers</a><br>
          <i class="uk-icon-check "></i>&nbsp; <a href="#">Door to door transfers</a><br>
          <i class="uk-icon-check "></i>&nbsp; <a href="#">Sightseeing tours</a></p>
      </div>
      <div class="col-md-3">
        <h3 class="header header-bottom">AIRPORT TRANSFERS</h3>
        <p> <i class="uk-icon-check "></i>&nbsp; <a href="Luton_Airport_Taxi.php">Luton Airport Taxi</a><br>
          <i class="uk-icon-check "></i>&nbsp; <a href="City_Airport_Tax.php">City Airport Taxi</a><br>
          <i class="uk-icon-check "></i>&nbsp; <a href="St_Pancras_Eurostar_Taxi.php">St Pancras Eurostar Taxi</a><br>
		   <i class="uk-icon-check "></i>&nbsp; <a href="Gatwick_Airport_Taxi.php">Gatwick Airport Taxi</a><br>
          <i class="uk-icon-check "></i>&nbsp; <a href="Stansted_Airport_Taxi.php">Stansted Airport Taxi</a><br>
          <i class="uk-icon-check "></i>&nbsp; <a href="Heathrow_Airport_Taxi.php">Heathrow Airport Taxi</a> </p>
      </div>
    </div>
    <div style="text-align: center;">
      <a href="ShareLink.php" class="btn btn-primary">Add Your Link</a><br>
    </div>
  </div>
</section>
<footer id="footer-wrapper">
  <div class="container">
    <div id="footer" class="row">
      <div class="col-md-4"> <span class="copyright">Copyright &copy;  2015 </span> </div>
      <div class="col-md-4 uk-text-center">
        <div> <a href="#" class="btn btn-inverse social"><i class="uk-icon-facebook"></i></a> <a href="#" class="btn btn-inverse social"><i class="uk-icon-twitter"></i></a> <a href="#" class="btn btn-inverse social"><i class="uk-icon-pinterest"></i></a> <a href="#" class="btn btn-inverse social"><i class="uk-icon-google-plus"></i></a> <a href="#" class="btn btn-inverse social"><i class="uk-icon-youtube-play"></i></a> </div>
        <a class="totop" rel="nofollow" href="#page-top" title="Goto Top"><i class="uk-icon-caret-up"></i></a> </div>
      <div class="col-md-4 uk-text-right"> <a href="about.php">About Us</a> | <a href="contact.php">Contact Us</a> | <a href="faq.php">FAQ</a> </div>
    </div>
  </div>
</footer>
</div>
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?471e7JoVHZ1sr53A38uuS6QGBREK1wcX";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<script src="js/jquery.nouislider.all.min.js"></script>
<script src="assets/js/scripts.js"></script>
<script src="assets/js/jquery.min.js"></script>

<script src="assets/js/jquery-ui.min.js"></script>
<script src="assets/js/uikit.min.js"></script>
<script src="assets/js/owl.carousel.min.js"></script>
<script src="assets/js/template.js"></script>


<script src="assets/js/ie10-viewport-bug-workaround.js"></script>
	<div class="modal fade" id="login_box" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
       <div class="modal-content" id="user_box">
       <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="login_modal_title">Login</h4>
        </div>
        <form action="" method="post" class="myurlform">
      		<div class="modal-body" id="login_modal_content">
            <label for="email_login">Email :</label>
      		  <input type="email" id="email_login" name="email_login" value="<?php if(isset($_SESSION["email"])!=''){ echo $_SESSION["email"]; } ?>"><br>
      			<label for="password_login">Password :</label>
      			<input type="password" id="password_login" name="password_login">
      		
      		  Forgot your password? <a href="#" class="forgot_pass_link">Request a new one.</a><br><br>
      		  <a href="booking_information.php">Sign up here.</a>
      		  <div class="modal-footer">
                <button type="button" name="login" class="myurl waves-effect waves-light btn popup_login" data-actiontype="login">Login</button> 
      		  &nbsp;&nbsp;&nbsp;
      		  <button type="button" class="waves-effect waves btn-flat" data-dismiss="modal">Close</button>
              </div>
              </div>
		
        
      		</form>
      		<form action="" method="post">
      		  <div class="modal-body" id="forgot_pass_modal_content" style="display:none">
               
      		  <label for="email_pwdreset">Enter your Email, we'll send your password:</label>
      			<input type="email" id="email_pwdreset" name="email_login"><br>
      			
      			  
      		  <a class="login_link" href="#">Login here.</a> or <a href="booking_information.php">Sign up here.</a>
      		   <div class="modal-footer">
                <button type="submit" class="waves-effect waves-light btn popup_login">Submit</button>&nbsp;&nbsp;&nbsp;
      		  <button type="button" class="waves-effect waves btn-flat" data-dismiss="modal">Close</button>
              </div>
              </div>
      		</form>
      </div>
      
    </div>
  </div>
  <script>
	
		
		
		$('.login_link').click(function(){
			$('#login_modal_title').html('Login');
			$('#login_modal_content').css('display', 'block');
			$('#signup_modal_content').css('display', 'none');
			$('#forgot_pass_modal_content').css('display', 'none');
			$('#modal_action_btn').html('Login');
			$('#modal_action_btn').attr('data-actiontype', 'login');
		});
		$('.forgot_pass_link').click(function(){
			$('#login_modal_title').html('Forgot password');
			$('#forgot_pass_modal_content').css('display', 'block');
			$('#login_modal_content').css('display', 'none');
			$('#signup_modal_content').css('display', 'none');
			$('#modal_action_btn').html('Send me the password');
			$('#modal_action_btn').attr('data-actiontype', 'pwdreset');
		});
		
		$('#modal_action_btn').click(function(){
			if($('#modal_action_btn').attr('data-actiontype') == "login"){
				
				
					$.post('login.php?ajax=true', {postLogin: true, email: $('#email_login').val(), password: $('#password_login').val()}, function(res){
						if(res == ""){
							$.post('ajax/user/main.php', {}, function(res2){
								$('#user_box').html(res2);
								$('#login_popover').css('background', '#F70977');
							});
							
						} else {
							alert(res);
						}
					});
				
			} 
		});
	
		
		</script>
		<script>
jQuery(document).ready(function() {
function count($this){
var current = parseInt($this.html(), 10);
current = current + 1; /* Where 1 is increment */
$this.html(++current);
if(current > $this.data('count')){
$this.html($this.data('count'));
} else {
setTimeout(function(){count($this)}, 15);
}
}
jQuery(".stat-count").each(function() {
jQuery(this).data('count', parseInt(jQuery(this).html(), 10));
jQuery(this).html('0');
count(jQuery(this));
});
});
</script>
</body>
</html>