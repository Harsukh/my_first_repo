<?php include("header.php"); ?>
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<?php include("php/SessionUnset.php"); ?>
<?php include("banner_inner.php"); ?>

<div class="container driver_container">
  <div class="row">
    <div class="col-md-12">
      <h2 class="driver">Driver's Information</h2>
      <br />
      <br />
      <h5 class="driver">Personal Information</h5>
    </div>
    <div id="kv-avatar-errors-2" class="center-block" style="width:800px;display:none"></div>
    <form id="registerform" class="form-horizontal" role="form" action="php/process_driver.php" method="post" enctype="multipart/form-data">
      <div class="col-md-4 left_side_form">
        <div class="form-group">
          <label>Title</label>
          <input type="text" class="form-control" name="title" value="" placeholder="Title" style="width:70%;" required>
        </div>
		<div class="form-group">
          <label>Name</label>
          <input type="text" class="form-control" name="name" value="" placeholder="Name" style="width:70%;" required>
        </div>
		<div class="form-group">
          <label>Surname</label>
          <input type="text" class="form-control" name="surname" value="" placeholder="Surname" style="width:70%;" required>
        </div>
        <div class="form-group">
          <label>DOB</label>
          <input name="datepicker" type="date" id="datepicker" class="form-control input-sm name" placeholder="Date of Birth" style="width:70%;" required>
        </div>
		<div class="form-group">
          <label>Address</label>
		  <input type="text" class="form-control" name="address1" value="" placeholder="Address Line 1" style="width:70%;" required><br />
          <input type="text" class="form-control" name="address2" value="" placeholder="Address Line 2" style="width:70%;" required>
         </div>
		 <div class="form-group">
          <label>Email</label>
          <input type="email" class="form-control" name="email" value="" placeholder="Email" style="width:70%;" required>
        </div>
		 <div class="form-group">
          <label>Post Code</label>
          <input type="text" class="form-control" name="postcode" value="" placeholder="Post Code" style="width:70%;" required>
        </div>
		
        <div class="form-group">
          <label>DVLA No</label>
          <input type="text" class="form-control" name="dvlano" value="" placeholder="DVLA No" style="width:70%;" required>
        </div>
        <div class="form-group">
          <label>PCO Badge</label>
          <input type="text" class="form-control" name="pco" value="" placeholder="PCO Badge" style="width:70%;" required>
        </div>
		<h3>Attach Documents</h3>
        <br />
        <div class="form-group">
          <label>Photocopy of DVLA </label>
          <input name="photo_dvla" type="file" class="file-loading" required>
        </div>
        <div class="form-group">
          <label>DVLA Counter Part </label>
          <input name="photo_dvla_counter" type="file" class="file-loading" required>
        </div>
        <div class="form-group">
          <label>PCO Badge </label>
          <input name="photo_pco" type="file" class="file-loading" required>
        </div>
        <div class="form-group">
          <label>PCO Counter Part </label>
          <input name="photo_pco_counter" type="file" class="file-loading" required>
        </div>
	</div>
      <div class="col-md-4 center_side_form">
        <div class="form-group ni_number">
          <label>N I Number</label>
          <input type="text" class="form-control" name="ninumber" value="" placeholder="N I Number" style="width:70%;" required>
        </div>
       <div class="expiry_main_div">
        <div class="form-group">
          <label>Expiry Date</label>
          <input name="dvlaexpirydate" type="date" id="dvlaexpirydate" placeholder="DVLA No Expiry Date" class="form-control input-sm name dvlaexpirydate" style="width:70%;" required>
        </div>
        <div class="form-group">
          <label class="pcoexpirydate_label">Expiry Date</label>
          <input name="pcoexpirydate" type="date" id="pcoexpirydate" placeholder="PCO Badge Expiry Date" class="form-control input-sm name pcoexpirydate" style="width:70%;" required>
        </div>
		</div>
      </div>
      <div class="col-md-4 right_side_form">
        <div class="form-group"> <img src="images/default_avatar_male.jpg" class="img-responsive" alt=""><br />
          <input id="avatar" name="avatar" type="file" class="file-loading" required>
        </div>
      </div>
    
      <div class="col-md-12">
        <div class="form-group">
          <center>
            <button type="submit" name="submit" class="btn btn-default driver_save">Save</button>
          </center>
        </div>
      </div>
    </form>
  </div>
</div>
<?php include("footer.php"); ?>
