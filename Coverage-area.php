<?php include("header.php"); ?>
<script src="js/jquery-1.10.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<?php include("php/SessionUnset.php"); ?>
<?php include("banner_inner.php"); ?>
<div class="about_container container">
		<h1>Coverage</h1>
			<p>We can pick you up from anywhere in the UK and take you to anywhere in the UK as far as it pre-booked via our booking
			   centre (Not with Driver).</p>
			<p>Airport Passengers will be met at the information desk, by the driver, who will hold a 24SE7EN CARZ board displaying
			   the details of the client’s name and flight number. If you cannot see our driver please contact our trained operator 
			   on 020 8749 1000, who will guide the driver and will remain on the line until they have successfully united you with
			   the driver.</p>
		<h2>Areas We Cover</h2>
			<h3>Airports: </h3>
				<table style="width:100%" class="table table-striped">
					<tr class="col">
						<td>Heathrow Airport</td>
						<td>Gatwick Airport</td> 
						<td>Stansted Airport</td>
					</tr>
					<tr>
						<td>Luton Airport	</td>
						<td>London city Airport</td> 
						<td>Biggin hill airport</td>
					</tr>
					<tr>
						<td>Southend airport</td>
						<td>Birmingham airport</td> 
						<td>Manchester airport</td>
					</tr>
					<tr>
						<td>Bristol airport	</td>
						<td>Southampton airport</td> 
						<td>Norwich airport</td>
					</tr>
					<tr>
						<td>Liverpool airport</td>
						<td>Cardiff airport</td> 
					</tr>
				</table>
			<h3>Cruise / Sea Ports:</h3>
				<table style="width:100%" class="table table-striped">
					<tr class="col">
						<td>Dover Sea / Cruise Port</td>
						<td>Harwich Cruise Port</td> 
						<td>Southampton Sea / Cruise Port</td>
					</tr>
					<tr>
						<td>Portsmouth Cruise Port	</td>
						<td>Tilbury cruise port</td> 
					</tr>
				</table>	
			<h3>Main Line Stations Covered:</h3>
				<table style="width:100%" class="table table-striped">
					<tr class="col">
						<td>Kings Cross </td>
						<td>St Pancras International</td> 
					</tr>
					<tr>
						<td>Victoria Coach Station</td>
						<td>Victoria Train Station</td> 
					</tr>
					<tr>
						<td>Paddington Station</td>
						<td>Liverpool Street Station</td> 
					</tr>
					<tr>
						<td>Waterloo Station</td>
						<td>Euston Train Station</td> 
					</tr>
				</table>	
			<h2>Postcodes Covered (London & Surroundings):</h2>
				<h1>London</h1>
			<h3>Central London / Greater London:</h3>
				<table style="width:100%" class="table table-striped">
					<tr class="col">
						<td>EC1</td>
						<td>Barbican, Clerkenwell, Finsbury Circle</td> 
					</tr>
					<tr>
						<td>EC2	</td>
						<td>Moorgate, Liverpool Street</td> 
					</tr>
					<tr>
						<td>EC3	</td>
						<td>Aldgate, Monument, Tower Hill</td> 
					</tr>
					<tr>
						<td>EC4</td>
						<td>Fleet Street, St Paul’s</td> 
					</tr>
					<tr>
						<td>WC1</td>
						<td>Bloomsbury, Gray’s Inn</td> 
					</tr>
					<tr>
						<td>WC2</td>
						<td>Covent Garden, Holborn, Strand</td> 
					</tr>
				</table>	
			<h3>West London:</h3>
				<table style="width:100%" class="table table-striped">
					<tr class="col">
						<td>W1</td>
						<td>, Soho, Mayfair, Piccadilly, Regent’s Street, Oxford Street, Baker Street</td> 
					</tr>
					<tr>
						<td>W2	</td>
						<td>Bayswater, Paddington</td> 
					</tr>
					<tr>
						<td>W3</td>
						<td>Acton</td> 
					</tr>
					<tr>
						<td>W4</td>
						<td>Chiswick</td> 
					</tr>
					<tr>
						<td>W5</td>
						<td>South Ealing, Ealing Broadway</td> 
					</tr>
					<tr>
						<td>W6</td>
						<td>Hammersmith</td> 
					</tr>
					<tr>
						<td>W7	</td>
						<td>Hanwell</td> 
					</tr>
					<tr>
						<td>W8</td>
						<td>South Kensington, Kensington High Street</td> 
					</tr>
					<tr>
						<td>W9</td>
						<td>Maida Vale, Warwick Avenue</td> 
					</tr>
					<tr>
						<td>W10	</td>
						<td>Ladbroke Grove, North Kensington</td> 
					</tr>
					<tr>
						<td>W11</td>
						<td>Notting Hill, Holland Park</td> 
					</tr>
					<tr>
						<td>W12</td>
						<td>Shepherd’s Bush</td> 
					</tr>
					<tr>
						<td>W13</td>
						<td>West Ealing</td> 
					</tr>
					<tr>
						<td>W14	</td>
						<td>West Kensington</td> 
					</tr>
				</table>	
			<h3>North London:</h3>
				<table style="width:100%" class="table table-striped">
					<tr class="col">
						<td>N1</td>
						<td>Islington, Barnsbury, Canonbury, Angel</td> 
					</tr>
					<tr>
						<td>N2</td>
						<td>East Finchley</td> 
					</tr>
					<tr>
						<td>N3</td>
						<td>Finchley Central</td> 
					</tr>
					<tr>
						<td>N4</td>
						<td>Finsbury Park, Manor House</td> 
					</tr>
					<tr>
						<td>N5</td>
						<td>Highbury, Arsenal</td> 
					</tr>
					<tr>
						<td>N6</td>
						<td>Highgate</td> 
					</tr>
					<tr>
						<td>N7</td>
						<td>Holloway</td> 
					</tr>
					<tr>
						<td>N8</td>
						<td>Crouch End, Hornsey</td> 
					</tr>
					<tr>
						<td>N9</td>
						<td>Lower Edmonton</td> 
					</tr>
					<tr>
						<td>N10</td>
						<td>Muswell Hill</td> 
					</tr>
					<tr>
						<td>N11</td>
						<td>Frien Barnet, New Southgate</td> 
					</tr>
					<tr>
						<td>N12</td>
						<td>North Finchley, Woodside Park</td> 
					</tr>
					<tr>
						<td>N13</td>
						<td>Palmers Green</td> 
					</tr>
					<tr>
						<td>N14</td>
						<td>Southgate</td> 
					</tr>
					<tr>
						<td>N15</td>
						<td>Seven Sisters</td> 
					</tr>
					<tr>
						<td>N16</td>
						<td>Stamford Hill, Stoke Newington</td> 
					</tr>
					<tr>
						<td>N17</td>
						<td>Tottenham, Tottenham Hale</td> 
					</tr>
					<tr>
						<td>N18</td>
						<td>Upper Edmonton</td> 
					</tr>
					<tr>
						<td>N19	</td>
						<td>Archway, Tufnell Park, Upper Holloway</td> 
					</tr>
					<tr>
						<td>N20</td>
						<td>Totteridge, Whetstone</td> 
					</tr>
					<tr>
						<td>N21</td>
						<td>Winchmore Hill</td> 
					</tr>
					<tr>
						<td>N22</td>
						<td>Wood Green, Alexandra Palace</td> 
					</tr>
				</table>	
			<h3>North West London:</h3>
				<table style="width:100%" class="table table-striped">
					<tr class="col">
						<td>NW1</td>
						<td>Camden Town, Regent’s Park</td> 
					</tr>
					<tr>
						<td>NW2</td>
						<td>Cricklewood, Child’s Hill, Willesden Green</td> 
					</tr>
					<tr>
						<td>NW3</td>
						<td>Hampstead, Neasden</td> 
					</tr>
					<tr>
						<td>NW4</td>
						<td>Brent Cross, Hendon</td> 
					</tr>
					<tr>
						<td>NW5	</td>
						<td>Kentish Town</td> 
					</tr>
					<tr>
						<td>NW6</td>
						<td>Kilburn, Queen’s Park, West Hampstead</td> 
					</tr>
					<tr>
						<td>NW7</td>
						<td>Mill Hill</td> 
					</tr>
					<tr>
						<td>NW8</td>
						<td>St John’s Wood</td> 
					</tr>
					<tr>
						<td>NW9</td>
						<td>Colindale, Kingsbury</td> 
					</tr>
					<tr>
						<td>NW10</td>
						<td>Harlesden, Kensal Rise, Willesden Junction, Neasden, North Action</td> 
					</tr>
					<tr>
						<td>NW11</td>
						<td>Golders Green, Hampstead Garden Suburb	</td> 
					</tr>
				</table>
			<h3>South West London:</h3>
				<table style="width:100%" class="table table-striped">
					<tr class="col">
						<td>SW1</td>
						<td>Belgravia, Pimlico, Westminster, Victoria</td> 
					</tr>
					<tr>
						<td>SW2</td>
						<td>Brixton, Streatham Hill</td> 
					</tr>
					<tr>
						<td>SW3</td>
						<td>Brompton, Chelsea</td> 
					</tr>
					<tr>
						<td>SW4</td>
						<td>Clapham</td> 
					</tr>
					<tr>
						<td>SW5</td>
						<td>Earl's Court</td> 
					</tr>
					<tr>
						<td>SW6</td>
						<td>Fulham, Parson's Green</td> 
					</tr>
					<tr>
						<td>SW7</td>
						<td>South Kensington, Knightsbridge</td> 
					</tr>
					<tr>
						<td>SW8</td>
						<td>Nine Elms, South Lambeth</td> 
					</tr>
					<tr>
						<td>SW9</td>
						<td>Brixton, Stockwell</td> 
					</tr>
					<tr>
						<td>SW10</td>
						<td>West Brompton, World's End</td> 
					</tr>
					<tr>
						<td>SW11</td>
						<td>Battersea, Clapham Junction</td> 
					</tr>
					<tr>
						<td>SW12</td>
						<td>Balham</td> 
					</tr>
					<tr>
						<td>SW13</td>
						<td>Barnes, Castelnau</td> 
					</tr>
					<tr>
						<td>SW14</td>
						<td>East Sheen, Mortlake</td> 
					</tr>
					<tr>
						<td>SW15</td>
						<td>Putney, Roehampton</td> 
					</tr>
					<tr>
						<td>SW16</td>
						<td>Norbury, Streatham</td> 
					</tr>
					<tr>
						<td>SW17</td>
						<td>Tooting</td> 
					</tr>
					<tr>
						<td>SW18</td>
						<td>Earlsfield, Wandsworth</td> 
					</tr>
					<tr>
						<td>SW19</td>
						<td>Merton, Wimbledon</td> 
					</tr>
					<tr>
						<td>SW20</td>
						<td>Raynes Park, South Wimbledon</td> 
					</tr>
				</table>	
			<h3>South East London:</h3>
				<table style="width:100%" class="table table-striped">
					<tr class="col">
						<td>SE1</td>
						<td>Bermondsey, Borough, Southwark, Waterloo</td> 
					</tr>
					<tr>
						<td>SE2</td>
						<td>Abbey Wood</td> 
					</tr>
					<tr>
						<td>SE3</td>
						<td>Blackheath, Westcombe Park</td> 
					</tr>
					<tr>
						<td>SE4</td>
						<td>Brockley, Crofton Park, Honor Oak Park</td> 
					</tr>
					<tr>
						<td>SE5</td>
						<td>Camberwell</td> 
					</tr>
					<tr>
						<td>SE6</td>
						<td>Bellingham, Catford, Hither Green</td> 
					</tr>
					<tr>
						<td>SE7</td>
						<td>Charlton</td> 
					</tr>
					<tr>
						<td>SE8</td>
						<td>Deptford</td> 
					</tr>
					<tr>
						<td>SE9</td>
						<td>Eltham, Mottingham</td> 
					</tr>
					<tr>
						<td>SE10</td>
						<td>Greenwich</td> 
					</tr>
					<tr>
						<td>SE11</td>
						<td>Lambeth</td> 
					</tr>
					<tr>
						<td>SE12</td>
						<td>Grove Park, Lee</td> 
					</tr>
					<tr>
						<td>SE13</td>
						<td>Hither Green, Lewisham</td> 
					</tr>
					<tr>
						<td>SE14</td>
						<td>New Cross, New Cross Gate</td> 
					</tr>
					<tr>
						<td>SE15</td>
						<td>Nunhead, Peckham</td> 
					</tr>
					<tr>
						<td>SE16</td>
						<td>Rotherhithe, South Bermondsey, Surrey Docks</td> 
					</tr>
					<tr>
						<td>SE17</td>
						<td>Elephant & Castle, Walworth</td> 
					</tr>
					<tr>
						<td>SE18</td>
						<td>Plumstead, Woolwich</td> 
					</tr>
					<tr>
						<td>SE19</td>
						<td>Crystal Palace, Upper Norwood</td> 
					</tr>
					<tr>
						<td>SE20</td>
						<td>Anerley, Penge</td> 
					</tr>
					
					<tr>
						<td>SE21</td>
						<td>Dulwich</td> 
					</tr>
					<tr>
						<td>SE22</td>
						<td>East Dulwich</td> 
					</tr>
					<tr>
						<td>SE23</td>
						<td>Forest Hill</td> 
					</tr>
					<tr>
						<td>SE24</td>
						<td>Herne Hill</td> 
					</tr>
					<tr>
						<td>SE25</td>
						<td>South Norwood</td> 
					</tr>
					<tr>
						<td>SE26</td>
						<td>Sydenham</td> 
					</tr>
					<tr>
						<td>SE27</td>
						<td>Tulse Hill, West Norwood</td> 
					</tr>
					<tr>
						<td>SE28</td>
						<td>Thamesmead</td> 
					</tr>
				</table>
			<h3>East London:</h3>
				<table style="width:100%" class="table table-striped">
					<tr class="col">
						<td>E1</td>
						<td>Mile End, Stepney, Whitechapel</td> 
					</tr>
					<tr>
						<td>E2</td>
						<td>Bethnal Green, Shoreditch</td> 
					</tr>
					<tr>
						<td>E3</td>
						<td>Bow, Bromley-by-Bow</td> 
					</tr>
					<tr>
						<td>E4</td>
						<td>Chingford, Highams Park</td> 
					</tr>
					<tr>
						<td>E5</td>
						<td>Clapton</td> 
					</tr>
					<tr>
						<td>E6</td>
						<td>East Ham, Beckton</td> 
					</tr>
					<tr>
						<td>E7</td>
						<td>Forest Gate, Upton Park</td> 
					</tr>
					<tr>
						<td>E8</td>
						<td>Hackney, Dalston</td> 
					</tr>
					<tr>
						<td>E9</td>
						<td>Hackney, Homerton</td> 
					</tr>
					<tr>
						<td>E10</td>
						<td>Leyton</td> 
					</tr>
					<tr>
						<td>E11</td>
						<td>Leytonstone</td> 
					</tr>
					<tr>
						<td>E12</td>
						<td>Manor Park</td> 
					</tr>
					<tr>
						<td>E13</td>
						<td>Plaistow</td> 
					</tr>
					<tr>
						<td>E14</td>
						<td>Isle of Dogs, Millwall, Poplar</td> 
					</tr>
					<tr>
						<td>E15</td>
						<td>Stratford, West Ham</td> 
					</tr>
					<tr>
						<td>E16</td>
						<td>Canning Town, North Woolwich</td> 
					</tr>
					<tr>
						<td>E17</td>
						<td>Walthamstow</td> 
					</tr>
					<tr>
						<td>E18</td>
						<td>South Woodford</td> 
					</tr>
					<tr>
						<td>E19</td>
						<td>Olympic Park, Stratford</td> 
					</tr>
				</table>	
			<h3>Outer London:</h3>
				<table style="width:100%" class="table table-striped">
					<tr class="col">
						<td>BROMLEY</td>
						<td>BR1, BR2, BR3, BR4, BR5, BR6, BR7, BR8</td> 
					</tr>
					<tr>
						<td>Croydon</td>
						<td>CR0, CR1, CR2, CR3, CR4, CR5, CR6, CR7, CR8</td> 
					</tr>
					<tr>
						<td>Dartford</td>
						<td>DA1, DA2, DA3, DA4, DA5, DA6, DA7, DA8, DA9, DA10, DA11, DA12, DA13, DA14, DA15, DA16, DA17, DA18</td> 
					</tr>
					<tr>
						<td>Enfield</td>
						<td>EN1, EN2, EN3, EN4, EN5, EN6, EN7, EN8, EN9, EN10, EN11</td> 
					</tr>
					<tr>
						<td>Harrow</td>
						<td>HA0, HA1, HA3, HA4, HA5, HA6, HA7, HA8, HA9</td> 
					</tr>
					<tr>
						<td>Ilford</td>
						<td>IG1, IG2, IG3, IG4, IG5, IG6, IG7, IG8, IG9, IG10, IG11</td> 
					</tr>
					<tr>
						<td>Kingston</td>
						<td>KT1, KT2, KT3, KT4, KT5, KT6, KT7, KT8, KT9, KT10, KT11, KT12, KT13, KT14, KT15, KT16, KT17, KT18, KT19, KT20, KT21, KT22, KT23, KT24</td> 
					</tr>
					<tr>
						<td>Romford</td>
						<td>RM1, RM2, RM3, RM4, RM5, RM6, RM7, RM8, RM9, RM10, RM11, RM12, RM13, RM14, RM15, RM16, RM17, RM18, RM19, RM20</td> 
					</tr>
					<tr>
						<td>Sutton</td>
						<td>SM1, SM2, SM3, SM4, SM5, SM6, SM7</td> 
					</tr>
					<tr>
						<td>Slough</td>
						<td>SL1, SL2, SL3, SL4, SL5</td> 
					</tr>
					<tr>
						<td>Twickenham</td>
						<td>TW1, TW2, TW3, TW4, TW5, TW6, TW7, TW8, TW9, TW10, TW11, TW12, TW13, TW14, TW15, TW16, TW17, TW18, TW19, TW20</td> 
					</tr>
					<tr>
						<td>Uxbridge</td>
						<td>UB1, UB2, UB3, UB4, UB5, UB6, UB7, UB8, UB9, UB10</td> 
					</tr>
					<tr>
						<td>Watford</td>
						<td>WD6, WD7, WD17, WD18, WD19, WD23, WD24, WD25</td> 
					</tr>
				</table>
</div>
<?php include("footer.php"); ?>