<?php include('head.php'); ?>
<link href="../assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/pages/css/profile-2.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$(document).ready(function()
{
      
    $(".vehicle_type").change(function()
    {
        var name=$(this).val();
        var dataString = 'name='+ name;
        //var=userid=document.getElementsByTagName('user_id').val();
        //var dataString = 'uid='+ userid;
        $('body').find('.vehicle_add').find('tr').remove();
        $.ajax
        ({
            type: "POST",
            url: "php/get_vehicle_data.php",
            data: dataString,
            cache: false,
            success: function(html)
            {
                
               $('body').find('.getdata').find('.vehicle_add').append(html);
            } 
        });
    });
    
});
</script>
<?php include('header.php'); ?>

<?php
    $id=$rowtop['id'];
    $query=mysql_query("SELECT * FROM driver_vehicle_info where user_id=$id");
    $vehicle=mysql_fetch_array($query);
?>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="dashboard.php">Home</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <span>Vehicle</span>
                            </li>
                        </ul>
                        
                    </div>
                    <!-- END PAGE HEADER-->
                    <div class="profile">
                        <div class="tabbable-line tabbable-full-width">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab"> vehicle Detels... </a>
                                </li>
                            </ul>
                            <?php if(isset($_SESSION['Success'])!=''){ ?>
                                <div class="alert alert-success fade in" style="margin-top:18px;">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                    <strong>Success!</strong> <?php echo $_SESSION['Success']; ?>
                                </div>
                                <?php 
                                unset($_SESSION["Success"]);
                                ?>
                            <?php } ?>
                            <?php if(isset($_SESSION['error'])!=''){ ?>
                                <div class="alert alert-success fade in" style="margin-top:18px;">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                                    <strong>error!</strong> <?php echo $_SESSION['error']; ?>
                                </div>
                                <?php 
                                unset($_SESSION["error"]);
                                ?>
                            <?php } ?>
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_1">
                                    <form action="php/update_vehicle.php" method="post" enctype="multipart/form-data" class="getdata">
                                    <input type="hidden" name="vid" value="<?php echo $vehicle['id']; ?>">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group well">
                                                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                                                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                                            <?php if($vehicle['image']!=""){ ?>
                                                                                <img src="../upload/vehicle/<?php echo $vehicle['image']; ?>" alt="" /> </div>
                                                                            <?php }else{ ?>
                                                                                 <img src="..\assets\global\img\adduser.png" alt="" /> </div>
                                                                            <?php } ?>
                                                                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                                            <div>
                                                                                <span class="btn default btn-file">
                                                                                    <span class="fileinput-new"> Select image </span>
                                                                                    <span class="fileinput-exists"> Change </span>
                                                                                    <input type="file" name="image"> </span>
                                                                                <a href="javascript:;" class="btn default fileinput-exists fileinput-new" data-dismiss="fileinput"> Remove </a>
                                                                            <?php if($vehicle['image']!=""){ ?>
                                                                                <a class="btn default" href="php/delete_vehicle_image.php?v_id=<?php echo $vehicle['id'] ?>"> Delete </a>
                                                                            <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="clearfix margin-top-10">
                                                                            <span class="label label-danger">NOTE! </span>
                                                                            <span> Maximum Uplode 2MB File</span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="margin-top-10 well">
                                                                        <button class="btn green" style="width:100%;" name="ChangeImage" value="Save Changes"> Save Changes </button>
                                                                       
                                                                    </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="row">
                                                <div class="col-md-8 profile-info">
                                                    
                                                </div>
                                                <!--end col-md-8-->
                                               
                                                <!--end col-md-4-->
                                            </div>
                                            <!--end row-->
                                            <div class="tabbable-line tabbable-custom-profile">
                                                <ul class="nav nav-tabs">
                                                   
                                                    <li class="active">
                                                        <a href="#tab_1_11" data-toggle="tab"> Vehicle Information</a>
                                                    </li>
                                                    <li>
                                                        <a href="#tab_1_22" data-toggle="tab"> Car Owner Details </a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <!--tab-pane-->
                                                <div class="tab-pane active" id="tab_1_11">
                                                        <div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th> Filed </th>
                                                                        <th>Descrition </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            Vehicle Registration No:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="registration" value="<?php echo $vehicle['registration']; ?>" placeholder="Enter Vehicle Registration No..." class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             Make of car:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="make" value="<?php echo $vehicle['make']; ?>" placeholder="Enter Make of car..." class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             Model of car:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="model" value="<?php echo $vehicle['model']; ?>" placeholder="Enter Model of car..." class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             Vehicle Type:    
                                                                        </td>
                                                                        <td> 
                                                                            <select class="form-control vehicle_type" name="vehicle_type">
                                                                                <option value="">.....Select Vehicle Type....</option>
                                                                            <?php 
                                                                                $qu=mysql_query("select * from vehicle_type");
                                                                                while($type=mysql_fetch_array($qu)){
                                                                            ?>
                                                                                <option value="<?php echo $type['name'] ?>" <?php if($vehicle['vehicle_type'] == $type['name']){ echo 'selected'; } ?>><?php echo $type['name'] ?></option>
                                                                            <?php } ?>
                                                                            </select>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             Vehicle Color:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="vehicle_color" value="<?php echo $vehicle['vehicle_color']; ?>" placeholder="Enter Vehicle Color..." class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    
                                                                </tbody>
                                                                <tbody class="vehicle_add">
                                                                    <tr>          
                                                                        <td>
                                                                             passanger capacity:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="passanger_capacity" value="<?php echo $vehicle['passanger_capacity']; ?>" placeholder="Enter passanger capacity..." class="form-control passanger" disabled>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             capacity of beg (25kg):    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="beg_20kg" value="<?php echo $vehicle['beg_20kg']; ?>" placeholder="Enter capacity of beg (20kg)..." class="form-control 20kg" disabled>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             capacity of beg (5kg):    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="beg_5kg" value="<?php echo $vehicle['beg_5kg']; ?>" placeholder="Enter capacity of beg (5kg)..." class="form-control 5kh" disabled>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane" id="tab_1_22">
                                                        <div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>Filed</th>
                                                                        <th>Descrition</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                     <tr>
                                                                        <td>
                                                                             Owner Name:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" placeholder="Vehicle Owner Name!!!" name="owner_name" class="form-control" value="<?php echo $vehicle['owner_name']; ?>">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             Vehicle Registered 1st line address:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" placeholder="Address Line One!!!" name="address1" class="form-control" value="<?php echo $vehicle['vehicle_registered_address1']; ?>">
                                                                        </td>
                                                                    </tr>
                                                                     <tr>
                                                                        <td>
                                                                             Vehicle Registered 1st line address:    
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" placeholder="Address Line tow!!!" name="address2" class="form-control" value="<?php echo $vehicle['vehicle_registered_address2']; ?>">
                                                                        </td>
                                                                    </tr>
                                                                     <tr>
                                                                        <td>
                                                                             County:  
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="county" value="<?php echo $vehicle['county']; ?>" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                     <tr>
                                                                        <td>
                                                                             Town:  
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="town" value="<?php echo $vehicle['town']; ?>" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                             postcode:  
                                                                        </td>
                                                                        <td> 
                                                                            <input type="text" name="post_code" value="<?php echo $vehicle['post_code']; ?>" class="form-control">
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            </form>
                                                        </div>
                                                    </div>

                                                    <!--tab-pane-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
            <!-- END QUICK SIDEBAR -->
        
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
<?php include('footer.php'); ?>
<script src="../assets/pages/scripts/profile.min.js" type="text/javascript"></script>
<script src="../assets/pages/scripts/form-validation.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>