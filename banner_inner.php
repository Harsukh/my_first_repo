<link href="date_picker/css/style.css" rel="stylesheet" type="text/css"/>
<script src="date_picker/js/jquery-2.1.4.min.js"></script>
<script src="date_picker/js/bootstrap.min.js"></script>
<link href="date_picker/css/bootstrap-datetimepicker.css" rel="stylesheet">
<script src="date_picker/js/moment-with-locales.js"></script>
<script src="date_picker/js/bootstrap-datetimepicker.js"></script>

<link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<link href="css/full-slider.css" rel="stylesheet">
<link href="css/materialize2.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">

<style type="text/css">
.date_input_stl{
    height: 50px;
    background:#073A29;
    border-radius: 10px 0px 0px 10px;
    color: #fff;
}
.btn-lg{
  float: right;
  
}
.btn-lg:hover{
  color: #fff;
}
.face_two{
  margin-bottom:10px; 
}
#booking_panel_left{
  z-index: 99;
}
</style>
<script type="text/javascript">
$(document).ready(function(){
  
  $('.swap').click(function() {

    
    var p1 = $('#1').val();
    var p2 = $('#2').val();
    if(p1==' ')
    {
      alert('Please Select Pickup Location');
    }else if (p2==' ') {
      alert('Please Select Dropoff Location');
    }else{

      $('body').find('.abc').remove();
      $('body').find('.a1').append('<option class="abc" value="'+ p2 +'"  selected="selected">'+ p2 +'</option>');
      $('body').find('.a2').append('<option class="abc" value="'+ p1 +'"  selected="selected">'+ p1 +'</option>');
    }
       
  });
 
  $('#submit_step1').click(function() {

      var p1 = $('#1').val();
      var p2 = $('#2').val();
      var email = $('#cust_email').val();
      var date1 = $('.date_input_stl').val();

      $('.face_two').show();

      if(email=='')
      {

        $('.face_two').hide();
        alert('Please Email Address');
        return false;

      }else if (p1==' ') {

        $('.face_two').hide();
        alert('Please Select Dropoff Location');
        return false;

      }else if (p2==' ') {

         $('.face_two').hide();
        alert('Please Select Pickup Location');
        return false;
       
      }else if (date1=='') {

        $('.face_two').show();
        alert('Please Select Date');
        return false;

      }else{

        $("#bookdateform").attr("action","php/GetsPecialDatePrice.php");
        $('body').find('#bookdateform').append('<input name="vehicle_type" type="hidden" value="'+ date1 +'">');
        $("#bookdateform").submit();
        return true;

      }
      return false;
  });

});
</script>
<header id="mycarousel" class="carousel slide">
<?php
session_start();
?>

<div class="carousel-inner">
  <div class="item active">
    <div class="fill" style="background-image:url('images/slider1/02.jpg');"></div>
  </div>
</div>
</header>
<div class="box-book row " id="booking_box_content">
  <div class="col-sm-6 booking-box animated fadeInUp" id="booking_panel_left">
    <div class="row main_header_bg_home">
      <div class="col-sm-12 booking-box-heading"> London Airport Transfer Solution </div>
      <div class="col-sm-12">
        <div class="row2 header-row head_second_content">
          <div class="col s2 step-1 extra-font-2-ucase second_size" align="center"> TYPE YOUR REQUEST </div>
          <div class="col s2" align="center"> <i class="fa fa-arrow-right" style="font-size: 291%;"></i> </div>
          <div class="col s2 step-2 extra-font-2-ucase second_size" align="center"> GET FREE QUOTE </div>
          <div class="col s2" align="center"> <i class="fa fa-arrow-right" style="font-size: 291%;"></i> </div>
          <div class="col s2 step-3 extra-font-2-ucase second_size" align="center"> BOOK YOUR JOURNEY </div>
        </div>
      </div>
    </div>
    <div class="row" style="background-color: white;border-bottom-left-radius: 60px;">
	<form action="" method="post" id="bookdateform">
      <div class="col-sm-12" style="padding-top: 20px;">
		<div class="row2">
          <div class="col s12 m12 l12">
            <input id="cust_email" type="email" name="email" value="<?php if(isset($_SESSION['email'])!=''){ echo $_SESSION['email']; } ?>" required>
          </div>
        </div>
        <div class="row2">
          <div class="input-field col s12 m12 l5 d">
            <label for="pickup" class="active">Pickup Location** </label>
			      <select name="pickup"  class="form-control select2-multiple" id="1">
              <option value=" " selected="selected">...........Select Pickup Location............</option>
              <?php 
                  $q1=mysql_query("SELECT * FROM pickup_location where level='1'");
                  while($level_1=mysql_fetch_array($q1))
                  {
              ?>
                  <optgroup class="a1"  label="<?php echo $level_1['name']; ?>">
                      <?php 
                          $name1=$level_1['name'];
                          $q2=mysql_query("SELECT * FROM pickup_location where level='2' AND realetion_ship='$name1'");
                          while($level_2=mysql_fetch_array($q2))
                          {
                      ?>
                          <?php if($name1 == 'All Poscodes') { ?>
                              <option value="HI"  disabled="disabled"><?php echo $level_2['name']; ?></option>
                              <?php 
                                  $name2=$level_2['name'];
                                  $q3=mysql_query("SELECT * FROM pickup_location where level='3' AND realetion_ship='$name2'");
                                  while($level_3=mysql_fetch_array($q3))
                                  {
                              ?>
                                  <option value="<?php echo $level_3['name']; ?>" <?php if(isset($_SESSION['pickup'])!=""){if($_SESSION['pickup']==$level_3['name']){echo 'selected';  } } ?>><?php echo $level_3['name']; ?></option>
                              <?php } ?>
                                  
                          <?php }else{ ?> 
                                 <option value="<?php echo $level_2['name']; ?>" <?php if(isset($_SESSION['pickup'])!=""){if($_SESSION['pickup']==$level_2['name']){echo 'selected'; } } ?>><?php echo $level_2['name']; ?></option>
                          <?php } ?>
                      <?php } ?>
                  </optgroup>
              <<?php } ?>
            </select>
          </div>
          <div class="input-field col s4 m3 l2 offset-s4 offset-m4 switch-btn-new" style="text-align:center;color: #1B87C2; letter-spacing:1px;">
            <p class="switch-btn  btn swap"  style="width: 77% !important;"><i class="fa fa-exchange switch-location"></i></p>
            <data class="swap-btn">SWAP</data>
          </div>
          <div class="input-field col s12 m12 l5 d">
            <label for="dropoff" class="active">Dropoff Location** </label>
			       <select name="dropoff"  class="form-control select2-multiple" id="2">
                <option value=" " selected="selected">............Select Dropoff Location............</option>
                <?php 
                    $q11=mysql_query("SELECT * FROM pickup_location where level='1'");
                    while($level_11=mysql_fetch_array($q11))
                    {
                ?>
                    <optgroup class="a2"  label="<?php echo $level_11['name']; ?>">
                        <?php 
                            $name1=$level_11['name'];
                            $q22=mysql_query("SELECT * FROM pickup_location where level='2' AND realetion_ship='$name1'");
                            while($level_22=mysql_fetch_array($q22))
                            {
                        ?>
                            <?php if($name1 == 'All Poscodes') { ?>
                                <option value="HI"  disabled="disabled"><?php echo $level_2['name']; ?></option>
                                <?php 
                                    $name2=$level_22['name'];
                                    $q33=mysql_query("SELECT * FROM pickup_location where level='3' AND realetion_ship='$name2'");
                                    while($level_33=mysql_fetch_array($q33))
                                    {
                                ?>
                                    <option value="<?php echo $level_33['name']; ?>" <?php if(isset($_SESSION['dropoff'])!=""){if($_SESSION['dropoff']==$level_33['name']){echo 'selected'; } } ?>><?php echo $level_33['name']; ?></option>
                                <?php } ?>
                                    
                            <?php }else{ ?> 
                                <option value="<?php echo $level_22['name']; ?>" <?php if(isset($_SESSION['dropoff'])!=""){if($_SESSION['dropoff']==$level_22['name']){echo 'selected'; } } ?>><?php echo $level_22['name']; ?></option>
                            <?php } ?>
                        <?php } ?>
                    </optgroup>
                <<?php } ?>
            </select>
           </div>
        </div>
        <div class="row2" style="margin-bottom: 20px;">
          <div class="col s12 m9 18" id="msg_box">  
                <div class="face_two" <?php if(isset($_SESSION['SpecialPricr'])!=''){ ?> style="width:100%;display: block;" <?php }else{ ?> style="width:100%;display: none;"  <?php } ?>>
                    <div class="form-group">
                        <div class="input-group date modi_fic" id="datetimepicker1">
                            <input type='text' value="<?php if(isset($_SESSION['SpecialPricr'])!=''){ echo $_SESSION["FullBookDate"];  } ?>" name="bookingdate" class="form-control date_input_stl" placeholder="Enter Your Bokking Date And Time..."/>
                            <span class="input-group-addon btn-bg" style="border-radius: 0px 10px 10px 0px;">
                                <span class="glyphicon glyphicon-calendar cal-form"></span>
                            </span>
                        </div>
                    </div>
                    <!--//script-->
                    <script type="text/javascript">
                        $(function () {
                            $('#datetimepicker1').datetimepicker();
                        });
                    </script>
                </div>
          </div>
          <div class="col s12 m12 l3" align="right" style="float:right">
              <button type="submit" id="submit_step1" name="booking" class="waves-effect waves-light waves btn-large">QUICK QUOTE</button>
          </div>
        </div>
        <div class="row2">
          <div class="col s12 m12 l12" align="left" style="color:#073A29; font-weight:bold;position: relative;left: 2%;bottom: 1em;"> Required** </div>
        </div>
      </div>
	  </form>
    </div>
  </div>
  <div class="col-sm-1 " style="width: 3.333333%;"></div>
  <div class="col-sm-5 booking-box booking-box2 animated fadeInUp" id="map_container_2" style="height: auto;margin-top: 15px;color: white;padding-bottom: 18px;">
    <div class="row2 header-row" id="map_container_1">
      <div class="col s12" style="text-align:center">
        <h3 class="get_free_h3">Get free transfer & taxi prices</h3>
      </div>
    </div>
    <div class="row2" id="map6">
      <div class="col s12 pad-13 " id="map_container"> </div>
    </div>
    <div class="row2 pad-13" id="map1">
      <div class="col s6 font-size-12"> <i class="custom-icon fa fa-exchange"></i> <a href="#main_container" class="color-white link" onClick="_load('private_door_to_door_transfers')"> Private door to door Transfers </a> </div>
      <div class="col s6 font-size-12"> <i class="custom-icon fa fa-group"></i> <a href="#main_container" class="color-white link" onClick="_load('special_and_corporate_events')"> Special and Corporate events </a> </div>
    </div>
    <div class="row2 pad-8" id="map2">
      <div class="col s6 font-size-12"> <i class="custom-icon fa fa-cab"></i> <a href="#main_container" class="color-white link" onClick="_load('advanced_cab_services')"> Advanced Cab services </a> </div>
      <div class="col s6 font-size-12"> <i class="custom-icon fa fa-bus pad-left-10"></i> <a href="#main_container" class="color-white link" onClick="_load('sight_seeing_tours')"> Sight Seeing Tours </a> </div>
    </div>
    <div class="row2 pad-8" id="map3">
      <div class="col s6 font-size-12"> <i class="custom-icon fa fa-child pad-left-10"></i> <a href="#main_container" class="color-white link" onClick="_load('school_services')"> School Services </a> </div>
      <div class="col s6 font-size-12"> <i class="custom-icon fa fa-money"></i> <a href="#main_container" class="color-white link" onClick="_load('best_transfer_prices')"> Best transfer prices </a> </div>
    </div>
    <div class="row2 box-e3sr3" id="map4">
      <div class="col s12 box-g34ytfby34">
        <div class="row top-blue-border">
          <div class="col s6"> <i class="custom-icon2 fa fa-check-square"></i> Best prices online </div>
          <div class="col s6"> <i class="custom-icon2 fa fa-check-square"></i> Airport transfer </div>
        </div>
        <div class="row bottom-blue-border">
          <div class="col s6"> <i class="custom-icon2 fa fa-check-square"></i> No obligation </div>
          <div class="col s6"> <i class="custom-icon2 fa fa-check-square"></i> Sea port transfer </div>
        </div>
      </div>
    </div>
    <div class="row2" id="map5">
      <div class="col s12">
        <a href="about.php" class="whoweare"><button class="waves-effect waves btn-large full-width roll-btn">WHO WE ARE</button></a>
      </div>
    </div>
  </div>
</div>
<script>
$('.carousel').carousel({
interval: 4000,
pause: 'none'
});
</script>
